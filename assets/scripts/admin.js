$(() => {
// jQuery document ready
  $('#addauthor').click(() => {
    let index = $('.author').last().clone().attr('name');
    index = parseInt(index.replace(/[^\d.]/g, ''), 10) + 1;
    $('.author').last().clone().attr('name', '_author[' + index + ']')
      .appendTo('#clone_container');
    return false;
  });
});
