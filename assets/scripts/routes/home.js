export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    function cardUp() {
     $('.wide-image .reveal').toggleClass('card-up');
     var moveUp = $('.wide-image .reveal').outerHeight();
     $('.wide-image .reveal').css("top", (moveUp*-1));
    }

    function cardDown(){
      $('.wide-image .reveal').toggleClass('card-up');
      $('.wide-image .reveal').css("top", 0);
    }

    $('.low-rollout .wide-image').on("mouseover", cardUp);
    $('.low-rollout .wide-image').on("mouseout", cardDown);
  },
};
