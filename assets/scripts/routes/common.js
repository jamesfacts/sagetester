export default {
  init() {
    // JavaScript to be fired on all pages

    // sm device drop-down menu -- expand the navbar
    $('.banner .navbar-toggler').click(function(){
      $('.banner .navbar-toggleable-sm').toggleClass('expanded');
    });

    // check if the expanded menu needs to be removed on resize
    $(window).on('resize', function(){
      var width = viewport().width;
    
      if (width >= 768) { 
      	$('.banner .navbar-toggleable-sm').removeClass('expanded'); 
      	$('.navbar-collapse').each(function() {
         $(this).collapse('hide');
        });

      }
    });

    function viewport() {
      var e = window, a = 'inner';
	
      if (!('innerWidth' in window )) {
         a = 'client';
         e = document.documentElement || document.body;
      }
    
      return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    }

    $('.search-toggle').click(function(){
      $('.search-wide').toggleClass('push-up');
    });

    $( '.single .entry-content a[href^="http"]' ).attr('target', '_blank');
    
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
