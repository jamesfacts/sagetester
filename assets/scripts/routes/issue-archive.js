export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired after the init JS

    function cardMove(reveal) {
        
        let moveUp = reveal.outerHeight(); //+ parseFloat(reveal.css("padding-bottom"));
        let direction = -1;
        
        if ( reveal.hasClass('card-up') ) {
            direction = 0;
        } 

        reveal.toggleClass('card-up');
        reveal.css("top", (moveUp * direction));        
    }


    // this full-coverage link is only visible on md+ sizes

    $('.full-coverage').on("mouseover", function(){
        let reveal = $( this ).parent('.clickable').next();
        cardMove(reveal);
    });

    $('.full-coverage').on("mouseout", function(){
        let reveal = $( this ).parent('.clickable').next();
        cardMove(reveal);
    });

    // when full-coverage link is display:none, this tap-to-reveal function takes over

    $('.clickable').on("click", function(){
    	let reveal = $( this ).next();
    	cardMove(reveal);
    })
    
  },
};


