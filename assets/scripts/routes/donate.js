export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS

    $("input.donate").click(function()
      { 
        var storeUrl = 'http://store.thebaffler.com/cart/849575901:' + parseInt(jQuery("input[name='amount']").val()); 
        window.open( storeUrl, '_blank');
      });

    var bird = $('.bird-flying');
    bird.removeClass('not-ie-speech'); // drop in the hidden slide position 1
      
    //call bubble-text toggle
    window.setTimeout(function(){
      bird.toggleClass('ie-speech-donate');
      ieBubbleToggle();
    }, 10000)

    function ieBubbleToggle() {
      bird.toggleClass('ie-speech-donate').toggleClass('ie-speech-please');
      window.setTimeout(ieBubbleToggle, 2000 );
    }

    if(typeof donations_create_script === 'undefined') {
      var donations_create_script = true;

      var myshopify_domain = 'the-baffler-magazine.myshopify.com';
      var product_ids = [346139657];

      var includes = '<script src="https://checkout.stripe.com/checkout.js"></script>\
                      <script type="text/javascript" src="https://js.stripe.com/v2/"></script>\
                      <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>\
                      <script src="https://da.boldapps.net/assets_embed/js/bootstrap.min.js"></script>\
                      <script async src="https://da.boldapps.net/donations/generate_donation?product_id='+product_ids.join('-')+'&shop_url='+myshopify_domain+'"></script>\
                      <link rel="stylesheet" type="text/css" href="https://da.boldapps.net/app_assets/css/donation_styles.css">';

      $('.da_div').parent().append('<a class="btn btn-shopify small donate-button" id="customButton" data-toggle="modal" data-target="#myModal" style="display:none;">Make Donation!</a>');
      $('.da_div:first').parent().append(includes);

    } 
    
    function boldMarkupTimer() {
      if (!$('.other_option_amount').length) {
        window.requestAnimationFrame(boldMarkupTimer);
      }else {
         $('.other_option_amount').attr('placeholder','$1,000,000');

         $('.donation_option_div > div').click(function(e){
          if ( $(e.target).hasClass('other_option') || $(e.target).hasClass('other_option_amount') ) {
            $('.payment_type_div').addClass('expanded');
          } else {
            $('.payment_type_div').removeClass('expanded');
          }
        });
       }
    }

    boldMarkupTimer();


    
    
  },
};