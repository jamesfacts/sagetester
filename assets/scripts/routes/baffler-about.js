export default {
  init() {
    // JavaScript to be fired on the about us page

    function viewport() {
      var e = window, a = 'inner';
	
      if (!('innerWidth' in window )) {
         a = 'client';
         e = document.documentElement || document.body;
      }
    
      return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    }

    var mobileLayout = viewport().width >= 768 ? false : true;
    //  var intitalTop = parseInt( $('.sticky').css('top') ); this doesn't work.
    var intitalTop = 90;
    var tweakAmt = 0;

    $(window).resize(function() {
    	mobileLayout = viewport().width >= 768 ? false : true;
    	mobileLayout ? $('.sticky').css('top', 0 ) : $('.sticky').css('top', intitalTop );
  	});

    // Custom 
    var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
      var stickyHeight = sticky.outerHeight();
      var stickyTop = stickyWrapper.offset().top;
      var footerFrame = $('.footer-frame').outerHeight();
  		var stickyOffset = parseInt(sticky.css('top'));

  		if ((scrollElement.scrollTop() == 0) && !mobileLayout) {
  			sticky.css('top', intitalTop);
  		}

      if (scrollElement.scrollTop() >= stickyTop){
        stickyWrapper.height(stickyHeight, 0);
        sticky.addClass("is-sticky");
        
        // check if we're in a mobile layout       

        if (!mobileLayout ) {
        	// how far away from the bottom of the page are we?
      		var bottomOffset = $(document).height() - sticky.outerHeight() - sticky.offset().top;

      		
          
          if ( bottomOffset < footerFrame || ( parseInt(sticky.css('top')) < intitalTop ) ) {
          	console.log("bottomOffset " + bottomOffset);
          	tweakAmt = footerFrame-bottomOffset;
          	console.log("tweak amount " + tweakAmt);
          	stickyOffset = parseInt(sticky.css('top'));

          	sticky.css('top', (stickyOffset - tweakAmt));
          	console.log("stick's top value " + sticky.css("top"));
          } 
        }



      }
      else {
        sticky.removeClass("is-sticky");
        stickyWrapper.height('auto');
      }
    };
  
    // Find all data-toggle="sticky-onscroll" elements
    $('[data-toggle="sticky-onscroll"]').each(function() {
      var sticky = $(this);
      var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
      sticky.before(stickyWrapper);
      sticky.addClass('sticky');
    
      // Scroll & resize events
      $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
        stickyToggle(sticky, stickyWrapper, $(this));
      });
    
      // On page load
      stickyToggle(sticky, stickyWrapper, $(window));
    });


  },
  finalize() {
   
  },
};
