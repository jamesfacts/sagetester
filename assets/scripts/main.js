/** import external dependencies */
import 'jquery';
import 'bootstrap';

/** import local dependencies */
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import bafflerAbout from './routes/baffler-about';
import single from './routes/single';
import donate from './routes/donate';
import postTypeArchiveIssue from './routes/issue-archive';

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
  /** All pages */
  common,
  /** Home page */
  home,
  /** About Us pages. */
  bafflerAbout,
  single,
  donate,
  postTypeArchiveIssue,
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
