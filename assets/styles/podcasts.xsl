<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title><xsl:value-of select="/rss/channel/title"/> RSS Feed</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <style type="text/css">
                    body {
                        font-family: Helvetica, Arial, sans-serif;
                        font-size: 14px;
                        color: #545454;
                        background: #EEEEEE;
                        line-height: 1.5;
                    }
                    .explanation {
                        font-style: italic;
                        font-size: 10px;
                        color: #9E9E9E;
                        text-align: center;
                        margin-bottom: 30px;
                    }
                    a, a:link, a:visited {
                        color: #00afec;
                        text-decoration: none;
                    }
                    a:hover {
                        color: #787878;
                    }
                    h1, h2, h3, p {
                        margin-top: 0;
                        margin-bottom: 20px;
                    }
                    h3 {
                        font-style: italic;
                    }
                    #content {
                        width: 700px;
                        margin: 0 auto;
                        background: #FFF;
                        padding: 30px;
                        box-shadow: 0px 0px 2px #5D5D5D;
                    }
                    #channel-image {
                        float: right;
                        width: 200px;
                        margin-bottom: 20px;
                    }
                    #channel-image img {
                        width: 200px;
                        height: auto;
                    }
                    #channel-header {
                        margin-bottom: 20px;
                    }
                    .channel-item {
                        clear: both;
                        border-top: 1px solid #E5E5E5;
                        padding: 20px;
                    }
                    .episode-image img {
                        width: 100px;
                        height: auto;
                        margin: 0 30px 15px 0;
                        border-radius: 5px;
                    }
                    .episode_meta {
                        font-size: 11px;
                        font-weight: bold;
                        font-style: italic;
                    }
                    .logo {
                    	text-align: center;
    									margin-bottom: 2em;
                    }
                </style>
            </head>
            <body>
                <div id="content">
	                	<div class="logo">
	            				<svg version="1.1" 
												 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
												 x="0px" y="0px" width="244px" height="32px" viewBox="0 0 244 32" style="enable-background:new 0 0 244 32;"
												 xml:space="preserve">
												<path d="M176.2,0h28.5v4.9h-22.8V14H200v4.7h-18.2V27h22.8v4.9h-28.5L176.2,0z M34.8,0c7.5,0,11.1,3.3,11.1,8.2v0.6
														c0,3.5-2,6.3-6.1,7.1c4.6,0.8,6.6,3.9,6.6,6.7v0.9c0,5-4,8.4-11.4,8.4H14.9V0H34.8z M34.8,18.1H20.6v9.1h14.6
														c3.9,0,5.3-1.9,5.3-4.2v-0.6C40.5,19.9,38.8,18.1,34.8,18.1L34.8,18.1z M35.1,4.8H20.6v9.1h14.1c3.9,0,5.7-1.9,5.7-4.4V9
														C40.4,6.7,39,4.8,35.1,4.8L35.1,4.8z M53.5,31.9h-5.9L62.4,0h5l15.1,31.9h-6l-3.6-8H57L53.5,31.9z M58.9,19.6h11.9l-6-13.2
														L58.9,19.6z M125,4.9h22.8V0h-28.5v31.9h5.7V18.7h18.2V14H125V4.9z M86.2,31.9h5.7V18.7H110V14H91.8V4.9h22.8V0H86.2L86.2,31.9z
														 M165.4,27h-22.1v4.9h27.8V0h-5.7L165.4,27z M232,18l12,14h-7l-11.2-13.2h-10.4v13.1h-5.7V0h17.8c8.6,0,12.1,3.3,12.4,8.6V9
														C240,14.6,237.6,17.7,232,18z M228.4,14.5c3.9,0,5.8-2,5.8-4.6V9.1c0-2.2-1.6-4.3-6-4.3h-12.8v9.7L228.4,14.5z M3.2,9.4h1.6V1.5
														h3.2V0.1H0v1.4h3.2V9.4z M7.8,11.2H6.1v3.9H1.9v-3.9H0.3v9.3h1.6v-4h4.2v4h1.7V11.2z M7.8,23.9v-1.4H0.3v9.3h7.5v-1.4H1.9v-2.7
														h4.5v-1.4H1.9v-2.4H7.8z"/>
											</svg>
										</div>
                    <p class="explanation">
                        This is an automatically generated podcast RSS feed, meant for consumption by podcast feed readers using the URL in the address bar.
                    </p>
                    <div id="channel-header">
                        <h1>
                            <xsl:if test="/rss/channel/image">
                                <div id="channel-image">
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="/rss/channel/image/link"/>
                                        </xsl:attribute>
                                        <img>
                                            <xsl:attribute name="src">
                                                <xsl:value-of select="/rss/channel/image/url"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="title">
                                                <xsl:value-of select="/rss/channel/image/title"/>
                                            </xsl:attribute>
                                        </img>
                                    </a>
                                </div>
                            </xsl:if>
                            <xsl:value-of select="/rss/channel/title"/>
                        </h1>
                        <p>
                            <xsl:value-of select="/rss/channel/description"/>
                        </p>
                        <p>
                            <a>
                                <xsl:attribute name="href">
                                    <xsl:value-of select="/rss/channel/link"/>
                                </xsl:attribute>
                                <xsl:attribute name="target">_blank</xsl:attribute>
                                <em>The Baffler</em> home &#x0226B;
                            </a>
                        </p>
                    </div>
                    <xsl:for-each select="/rss/channel/item">
                        <div class="channel-item">
                            <h2>
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="link"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="target">_blank</xsl:attribute>
                                    <xsl:value-of select="title"/>
                                </a>
                            </h2>
                            <xsl:if test="description">
                                <p>
                                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                                </p>
                            </xsl:if>
                            <p class="episode_meta">
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="enclosure/@url"/>?ref=download
                                        </xsl:attribute>
                                        Download episode
                                    </a> |
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="enclosure/@url"/>?ref=new_window
                                        </xsl:attribute>
                                        <xsl:attribute name="target">_blank</xsl:attribute>
                                        Play in new window
                                    </a> |
                                    File size: <xsl:value-of select='format-number(number(enclosure/@length div "1024000"),"0.0")'/>MB
                            </p>
                        </div>
                    </xsl:for-each>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>