<?php

namespace App;


function relatedBox( $atts ) {
    $atts = shortcode_atts( ['connect-to' => '',
    												 'hed' => ''], $atts );
    $html = '<aside class="related-box">';

    if ( strval($atts['connect-to']) == strval(intval($atts['connect-to'])) ) {
    	$relatedPost = get_post($atts['connect-to']);
    	$html .= '<h3>More</h3>';
    	if ( has_post_thumbnail( $relatedPost->ID ) ) {
	    	$html .= '<div class="image-fill" style="background-image: url(';
	    	$html .= get_the_post_thumbnail_url( $atts['connect-to'], 'w350' ) .' );">';
				$html .= '<a class="full-coverage" href="' . get_permalink( $atts['connect-to'] ) .'"></a>';
				$html .= '</div>';
			}
    	$html .= '<h5><a href="' . get_permalink( $atts['connect-to'] ) . '?utm_campaign=bonus&utm_source=' . basename(get_permalink()) .'">' . $atts['hed'] . '</a></h5>';
		} else {
			$html .= '<p>Please double-check post ID</p>';
		}

		$html .= '</aside>';

    return $html;
}

add_shortcode('related-box', 'App\\relatedBox');


function boldDonateOne() {
    $html = '<div class="da_div" style="width: 100%;" data-product-id="346139657" data-shop-url="the-baffler-magazine.myshopify.com"></div>';
    $html .= '<a class="btn btn-shopify small donate-button" id="customButton" data-toggle="modal" data-target="" style="">Make Donation!</a>';

    return $html;
}

add_shortcode('bold-donate-one', 'App\\boldDonateOne');

