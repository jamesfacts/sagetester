<?php

namespace App;

/**
 * Navigation arguments
 *
 * @param $data
 *
 * @return mixed
 */
function navControl( $data ) {

	// Main Nav
	$mainNavArgs = ['theme_location'   => 'primary_navigation', 
	                'menu_id'          => false,
	                'menu_class'       => 'navbar-nav'];

	$data['mainNavArgs'] = $mainNavArgs;

	// Social Nav
	$footerNavArgs = ['theme_location' => 'footer_navigation', 
	                  'menu_class' => 'nav', 
	                  'items_wrap' => footer_nav_wrap() ];

	$data['footerNavArgs'] = $footerNavArgs;

return $data;
}


function homeQueries ( $data ) {

// TODO I want to place queries for home items here

	return $data;
}


function aboutSidebar ( ) {

// TODO I want to figure out what pages we need a sidebar on here... 

}



add_filter( 'sage/template/global/data', 'App\\navControl' );


