<?php

namespace App;

use Illuminate\Contracts\Container\Container as ContainerContract;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Config;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    // wp_enqueue_style('sage/podcasts.css', asset_path('styles/podcasts.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    // wp_register_style( 'graphik', get_template_directory_uri() . '/fonts/Graphik-Medium-Web.eot', false, null );

}, 100);


// add_action('wp_enqueue_scripts', function () {
//     wp_enqueue_script('hs_email', '//js.hsforms.net/forms/v2.js', null, null, true);
// }, 110);

add_action( 'admin_enqueue_scripts', function () {
   wp_enqueue_style('sage/admin_css', asset_path('styles/admin.css'), false, null);
   wp_enqueue_script('sage/admin_js', asset_path('scripts/admin.js'), false, null);
} );

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');
    add_theme_support('automatic-feed-links');

    // stop outputting main comments feed in head
    add_filter( 'feed_links_show_comments_feed', '__return_false' );

    // disable 'extras' feeds -- no filter available for this as of 4.7.5
    remove_action( 'wp_head', 'feed_links_extra', 3);

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'footer_navigation' => __('Footer Navigation', 'sage'),
        'about_navigation' => __('About Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // this supports the image in home 'big_story' ... maybe single post salvo, too?
    add_image_size('w700', 700, 9999);

    // this supports the image in three top rollouts, also the two right in the lower half of the six rollour
    add_image_size('w350', 350, 9999);

    // featured issue image homepage
    add_image_size('w450', 450, 9999);

    // big eye appeal image
    add_image_size('w500', 500, 9999);

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary',
        'before_widget' => '<nav class="navbar navbar-toggleable-sm %1$s %2$s" data-toggle="sticky-onscroll"><header class="menu-controls"><button class="navbar-toggler collapsed hidden-md-up" type="button" data-toggle="collapse" data-target="#sidebarToggle" aria-controls="sidebarToggle" aria-expanded="false" aria-label="Toggle navigation"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve" class="sub-plus"><g> <polygon class="st0" points="14,30 16,30 16,16 30,16 30,14 16,14 16,0 14,0 14,14 0,14 0,16 14,16 "/></g></svg></button>',
        'after_widget'  => '</div></nav>',
        // 'class'         => 'fixed-top',
        'before_title'  => '<h2 class="about-menu">',
        'after_title'   => '</h2></header><div class="collapse navbar-collapse" id="sidebarToggle">'
    ]);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

class nbt_Store_Item extends \WP_Widget {

  public function __construct() {
    $widget_options = array( 'classname' => 'store', 'description' => 'Pick an item for the store' );
    parent::__construct( 'store_widget', 'Footer Store', $widget_options );
  }

  public function widget( $args, $instance ) {
    $description = apply_filters( 'widget_text', $instance[ 'description' ] );
    $url = apply_filters( 'widget_text', $instance[ 'url' ] );
    $image = apply_filters( 'widget_text', $instance[ 'image' ] );

    $html = $args['before_widget'];
    $html .= '<div class="cover-image"><a href="' . $url .'"><img src="' . $image . '" alt="Baffler Store"/></a></div>';
    $html .= '<div class="meta">' . $args['before_title'] . '<a href="https://store.thebaffler.com/">Baffler Store</a>' . $args['after_title'];
    $html .= '<p class="description"><a href="' . $url . '"">' . $description . '</a></p></div>';
    $html .= $args['after_widget'];

    echo $html;
  }

  public function form( $instance ) {
    $description = ! empty( $instance['description'] ) ? $instance['description'] : '';
    $url = ! empty( $instance['url'] ) ? $instance['url'] : '';
    $image = ! empty( $instance['image'] ) ? $instance['image'] : '' ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'description' ); ?>">Item description:</label>
      <input type="text" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" value="<?php echo esc_attr( $description ); ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'url' ); ?>">URL to store:</label>
      <input type="text" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" value="<?php echo esc_attr( $url ); ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'image' ); ?>">URL to item image:</label>
      <input type="text" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" value="<?php echo esc_attr( $image ); ?>" />
    </p><?php
  }

  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance[ 'description' ] = strip_tags( $new_instance[ 'description' ] );
    $instance[ 'url' ] = strip_tags( $new_instance[ 'url' ] );
    $instance[ 'image' ] = strip_tags( $new_instance[ 'image' ] );
    return $instance;
  }

}

class nbtFooterCover extends \WP_Widget {

  public function __construct() {
    $widget_options = array( 'classname' => 'cover', 'description' => 'Cover image in footer' );
    parent::__construct( 'cover_widget', 'Footer Cover', $widget_options );
  }

  public function widget( $args, $instance ) {
    $html = $args['before_widget'];
    $cover = apply_filters( 'widget_text', $instance[ 'cover' ] );


    $html .= '<div class="cover-image"><a href="https://thebaffler.com/subscribe"><img src="' . $cover . '" alt="Subscribe Now!"></a></div>';
    $html .= $args['before_title'] . '<a href="https://thebaffler.com/subscribe">Subscribe Now!</a>' . $args['after_title'] . $args['after_widget'];

    echo $html;
  }

  public function form( $instance ) {
    $cover = ! empty( $instance['cover'] ) ? $instance['cover'] : '' ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'cover' ); ?>">Footer cover URL:</label>
      <input type="text" id="<?php echo $this->get_field_id( 'cover' ); ?>" name="<?php echo $this->get_field_name( 'cover' ); ?>" value="<?php echo esc_attr( $cover ); ?>" />
    </p><?php
  }

  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance[ 'cover' ] = strip_tags( $new_instance[ 'cover' ] );
    return $instance;
  }

}

add_action( 'widgets_init', function() {
  register_widget( __NAMESPACE__ . '\\nbtFooterCover' );
  register_widget( __NAMESPACE__ . '\\nbt_Store_Item' );
});


/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});




/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Sage config
     */
    $paths = [
        'dir.stylesheet' => get_stylesheet_directory(),
        'dir.template'   => get_template_directory(),
        'dir.upload'     => wp_upload_dir()['basedir'],
        'uri.stylesheet' => get_stylesheet_directory_uri(),
        'uri.template'   => get_template_directory_uri(),
        'dir.root'       => ABSPATH
    ];
    $viewPaths = collect(preg_replace('%[\/]?(templates)?[\/.]*?$%', '', [STYLESHEETPATH, TEMPLATEPATH]))
        ->flatMap(function ($path) {
            return ["{$path}/templates", $path];
        })->unique()->toArray();
    config([
        'assets.manifest' => "{$paths['dir.stylesheet']}/dist/assets.json",
        'assets.uri'      => "{$paths['uri.stylesheet']}/dist",
        'view.compiled'   => "{$paths['dir.root']}/cache/compiled",
        'view.namespaces' => ['App' => WP_CONTENT_DIR],
        'view.paths'      => $viewPaths,
    ] + $paths);

    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (ContainerContract $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view'], $app);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return '<?= App\\asset_path(\''.trim($asset, '\'"').'\'); ?>';
    });
});



// Standard WP gallery isn't styled in NBT

remove_shortcode('gallery', 'gallery_shortcode');

function contributor_func( $atts ) {
    $atts = shortcode_atts( array(
        'id' => '',
        'url' => ''
    ), $atts );

    //
    // get url assigned to author's profile
    //
    //$url = get_permalink($atts['id']) ? get_permalink($atts['id']) : '#';
    $url = get_post_meta($atts['id'], '_website', true) ? get_post_meta($atts['id'], '_website', true) : '#';

    //$html = '<a class="author_copyright" href="' . $url . '">© ' . get_the_title($atts['id']) .'</a>';
    $html = '<a target="_blank" class="author_copyright" href="' . $url . '">© ' . get_the_title($atts['id']) .'</a>';

    return $html;
}
add_shortcode( 'contributor', 'App\\contributor_func' );


/*
Plugin Name: Nested image caption shortcode
Plugin URI: https://github.com/tychay/ics_nested
Description: Allows caption shortcode to have shortcodes
Version: 1.0
Author: tychay
Author URI: http://terrychay.com/
License: GPL v2.0 or newer
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

function ics_nested_filter( $html, $attr, $content ) {
    $atts = shortcode_atts( array(
        'id'      => '',
        'align'   => 'alignnone',
        'width'   => '',
        'caption' => '',
        'class'   => '',
    ), $attr, 'caption' );

    $atts['width'] = (int) $atts['width'];
    if ( empty( $atts['caption'] ) ) {
      return do_shortcode( $content );
    }
    // patch the fact that the original prunes out caption
    if ( $atts['width'] < 1 ) {
        return do_shortcode( $content . ' ' . $atts['caption'] );
        // not perfect because we lost everything before $content and also the
        // trim removed stuff.
    }

    if ( ! empty( $atts['id'] ) )
        $atts['id'] = 'id="' . esc_attr( $atts['id'] ) . '" ';

    $class = trim( 'wp-caption ' . $atts['align'] . ' ' . $atts['class'] );

    if ( current_theme_supports( 'html5', 'caption' ) ) {
        return '<figure ' . $atts['id'] . 'style="width: ' . (int) $atts['width'] . 'px;" class="' . esc_attr( $class ) . '">'
        . do_shortcode( $content ) . '<figcaption class="wp-caption-text">' . do_shortcode( $atts['caption'] ) . '</figcaption></figure>';
    }

    $caption_width = 10 + $atts['width'];

    /**
     * Filter the width of an image's caption.
     *
     * By default, the caption is 10 pixels greater than the width of the image,
     * to prevent post content from running up against a floated image.
     *
     * @since 3.7.0
     *
     * @see img_caption_shortcode()
     *
     * @param int    $caption_width Width of the caption in pixels. To remove this inline style,
     *                              return zero.
     * @param array  $atts          Attributes of the caption shortcode.
     * @param string $content       The image element, possibly wrapped in a hyperlink.
     */
    $caption_width = apply_filters( 'img_caption_shortcode_width', $caption_width, $atts, $content );

    $style = '';
    if ( $caption_width )
        $style = 'style="width: ' . (int) $caption_width . 'px" ';

    return '<div ' . $atts['id'] . $style . 'class="' . esc_attr( $class ) . '">'
    . do_shortcode( $content ) . '<p class="wp-caption-text">' . do_shortcode( $atts['caption'] ) . '</p></div>';
}

// run a little earlier so other processors can do their thang.
add_filter( 'img_caption_shortcode', 'App\\ics_nested_filter', 5, 3 );



/**
 * Instruct Wordpress to resolve permalinks from custom posts with no URL prefix
 */


function na_parse_request( $query ) {

    if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', [ 'post', 'article', 'page' ] );
    }
}

add_action( 'pre_get_posts',  __NAMESPACE__ . '\\na_parse_request' );



/**
  *  Wrap footer menu with a copyright li
  */

function footer_nav_wrap() {
  // default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'

  // open the <ul>, set 'menu_class' and 'menu_id' values
  $wrap  = '<ul id="%1$s" class="%2$s">';
  $date  = date('Y');

  // get nav items as configured in /wp-admin/
  $wrap .= '%3$s';

  // the static link
  $wrap .= '<li class="menu-item static-copy hidden-md-up">&copy; The Baffler ' .$date . '</li>';

  // close the <ul>
  $wrap .= '</ul>';
  // return the result
  return $wrap;
}

function baffler_author_url($post_id) {
    $author_id = get_post_meta($post_id, '_author', true);
    if(!empty($author_id)) {
        // get author
        return get_permalink($author_id);
    }
}

function baffler_author_meta($post_id, $link = true, $homepage = false) {
    $author_array = get_post_meta($post_id, '_author');
    $class = 'class="name"';
    // $authorUrl = baffler_author_url($authors);

    if(!empty($author_array) && ($author_array != $post_id)) {
        $authors = $author_array[0];
        if(is_array($authors)) {
            $content = '';
            $comma = '';
            foreach($authors as $author) {
                $_author = get_post($author);
                    if($link != true) {
                        $content .= $comma . apply_filters('the_title', $_author->post_title);
                    } else {
                        $content .= $comma . '<a ' . $class . ' href="' . get_permalink($author) . '">' . apply_filters('the_title', $_author->post_title) . '</a>';
                    }
                $comma = ', ';
                if($homepage = true) {
                    $class = 'class="home-author-name"';
                }
            }
        } else {
            $author = get_post($authors);
            if($link != true) {
                $content = apply_filters('the_title', $author->post_title);
            } else {

                $content = '<a ' . $class . ' href="' . $authorUrl . '">' . apply_filters('the_title', $author->post_title) . '</a>';
            }
        }
    } else {
        $content = '';
    }
    return $content;
}

function featured_image_check ($min_width = 350, $min_height = 350) {

  if ( has_post_thumbnail( get_the_ID() ) ) {
      $thumb = array();
      $thumb_id = get_post_thumbnail_id( get_the_ID() );

      // first grab all of the info on the image... title/description/alt/etc.
      $args = array(
          'post_type' => 'attachment',
          'include' => $thumb_id
      );
      $thumbs = get_posts( $args );
      if ( $thumbs ) {
          // now create the new array
          $thumb['title'] = $thumbs[0]->post_title;
          $thumb['description'] = $thumbs[0]->post_content;
          $thumb['caption'] = $thumbs[0]->post_excerpt;
          $thumb['alt'] = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
          $thumb['sizes'] = array(
              'full' => wp_get_attachment_image_src( $thumb_id, 'full', false )
          );
          // add the additional image sizes
          foreach ( get_intermediate_image_sizes() as $size ) {
              $thumb['sizes'][$size] = wp_get_attachment_image_src( $thumb_id, $size, false );
          }
      } // end if

      // display the 'custom-size' image
      $height = $thumb['sizes']['full'][2];
      $width = $thumb['sizes']['full'][1];

      if (($width >= $min_width) && ($height >= $min_height)) {
        return true;
      }
  } else { // end if
    return false;
  }
}

function tall_featured_image_check ($min_width = 350, $min_height = 500) {

  if ( has_post_thumbnail( get_the_ID() ) ) {
      $thumb = array();
      $thumb_id = get_post_thumbnail_id( get_the_ID() );

      // first grab all of the info on the image... title/description/alt/etc.
      $args = array(
          'post_type' => 'attachment',
          'include' => $thumb_id
      );
      $thumbs = get_posts( $args );
      if ( $thumbs ) {
          // now create the new array
          $thumb['title'] = $thumbs[0]->post_title;
          $thumb['description'] = $thumbs[0]->post_content;
          $thumb['caption'] = $thumbs[0]->post_excerpt;
          $thumb['alt'] = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
          $thumb['sizes'] = array(
              'full' => wp_get_attachment_image_src( $thumb_id, 'full', false )
          );
          // add the additional image sizes
          foreach ( get_intermediate_image_sizes() as $size ) {
              $thumb['sizes'][$size] = wp_get_attachment_image_src( $thumb_id, $size, false );
          }
      } // end if

      // display the 'custom-size' image
      $height = $thumb['sizes']['full'][2];
      $width = $thumb['sizes']['full'][1];

      if (($width >= $min_width) && ($height >= $min_height) && (($height / $width) >= .7 ) ) {
        return true;
      }
  } else { // end if
    return false;
  }
}

function get_column_title () {
  $cats = get_the_category(get_the_ID());
  foreach ($cats as $cat ) {

    $catValues = get_term_meta( $cat->term_id, '_column' );

    foreach ($catValues as $key => $columnOpt) {
      if($columnOpt == '1') {
        $slugStr = '';
        if ( $cat->parent > 0 ) {
          $parentCat = get_category($cat->parent);
          $slugStr = $parentCat->slug . "/" . $cat->slug;
        } else {
          $slugStr = $cat->slug;
        }
        $column = (object) [
          'slug' => $slugStr,
          'name' => $cat->name
        ];
        return $column;
      }
    }

  }
  return false;
}

function get_all_columns () {
  $cats = get_categories();
  $columns = [];

  foreach ( $cats as $cat ) {
    $isColumn = get_term_meta( $cat->term_id, '_column' ) ? true : false;
    if ( $isColumn ) {
      $columns[] = $cat->slug;
    }
  }

  return $columns;
}

function get_featured_image_caption () {
  $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
  $attachment = get_post( $post_thumbnail_id );
  $caption = $attachment->post_excerpt;
  $caption = do_shortcode($caption, false);
  $caption = wp_kses( $caption, [
                      'a' => [
                        'href' => [],
                        'title' => [],
                        'target' => []
                      ]
            ]);
  if ($caption) {
    echo force_balance_tags($caption);
  }
}

function get_author_bio_tag () {

  if (get_post_meta(get_the_ID(), '_author', true)) {
      $author_id = get_post_meta(get_the_ID(), '_author', true);
      // $author_url = baffler_author_url(get_the_ID());
      $author_name = get_post_meta(get_the_ID(), '_author', true) ? get_the_title($author_id) : '';
      if (is_array($author_id)) {
          $author_bio = '';
          foreach ($author_id as $single_author) {
            $extended_author_content = get_post_field( 'post_content', $single_author );
            $author_content_parts = get_extended( $extended_author_content );

            // Output part before <!--more--> tag
            $author_bio .= $author_content_parts['main'];
          }
      } else {
          $extended_author_content = get_post_field( 'post_content', $single_author );
          $author_content_parts = get_extended( $extended_author_content );

          // Output part before <!--more--> tag
          $author_bio .= $author_content_parts['main'];
      }
  }
  return $author_bio;

}

function catSubhed () {
  if ( !is_404() ) {
    $category = get_the_category();
    $category_id = $category[0]->term_id;
    return category_description( $category_id ) ? category_description( $category_id ) : '';
  }

}

function get_issue_number () {

  $issue_id = get_post_meta(get_the_ID(), '_issue_id' );

  if ($issue_id) {
    $issue = get_post($issue_id[0]);
    $issue_number = $issue->post_title;

    return $issue_number;
  }

  return false;

}

function get_issue_slug () {

  $issue_id = get_post_meta(get_the_ID(), '_issue_id' );

  if ($issue_id) {
    $issue = get_post($issue_id[0]);
    $issue_slug = $issue->post_name;
    return $issue_slug;
  }

  return false;

}

function get_issue_date () {

  $issue_id = get_post_meta(get_the_ID(), '_issue_id' );

  if ($issue_id) {
    $issue_date = get_the_date( 'F Y', $issue_id[0] );

    return $issue_date;
  }

  return false;
}

function get_conditional_date () {

  $issue_meta = get_post_meta(get_the_ID(), '_issue_id' );
  $issue_id = 0;

  foreach ($issue_meta as $key => $issue) {
    $issue_id = intval($issue);
  }

  $s_post_date = ($issue_id > 0) ?  get_the_date( 'Y-m-d', $issue_id ): get_the_date( 'Y-m-d', get_the_ID() );
  $s_post_date_obj = strtotime( $s_post_date );

  if( $s_post_date_obj < strtotime('-6 months') ) {
    return ( $issue_id > 0 ) ? get_the_date( 'F j, Y', $issue_id) : get_the_date('F j, Y');
  } else {
    return get_the_date('F j');
  }
  return false;
}

function get_content_type () {
  $content_type = 'Word Factory';

  if ( get_post_type( get_the_ID() ) == 'issue' ) {
    $content_type = 'issue';
  }

  if ( get_post_type( get_the_ID() ) == 'book' ) {
    $content_type = 'book';
  }

  if ( get_post_type( get_the_ID() ) == 'event' ) {
    $content_type = 'event';
  }

  if ( get_post_type( get_the_ID() ) == 'baffler_author' ) {
    $content_type = 'contributor';
  }

  if ( get_post_type( get_the_ID() ) == 'page' ) {
    $content_type = 'page';
  }

  if ( get_post_type( get_the_ID() ) == 'article' ) {
    foreach (wp_get_object_terms( get_the_ID(), 'content_type' ) as $key => $type) {
      $content_type = $type->name;
    }
  }

  return strtolower($content_type);
}

function vert_text ($input_str) {

  $string_arr = str_split($input_str);
  $prepped_content = [];

  foreach ($string_arr as $letter) {
    $prepped_content[] =  $letter . '<br>';
  }

  $output = implode(" ",$prepped_content);
  return $output;
}



function custom_query_vars( $query ) {
      if ( !is_admin() && $query->is_main_query() ) {
        if ( is_post_type_archive( 'issue' ) ) {
            if ( get_query_var('paged') == 0 ) {
                $query->set( 'posts_per_page', 11 );
            } else {
                $query->set( 'posts_per_page', 12 );
            }
        } elseif ( is_search() ) {
            $query->set('post_type', ['post', 'article', 'page', 'baffler_author', 'event']);

        }
    }
}

add_action( 'pre_get_posts', 'App\\custom_query_vars' );


add_filter( 'posts_search_orderby', function( $search_orderby ) {
    global $wpdb;
    return "{$wpdb->posts}.post_type LIKE 'baffler_author' DESC, {$search_orderby}";
});




/***********************************************************
************************************************************
************************************************************/
add_action('after_setup_theme', function () {

  /**
   * Create @posts Blade directive
   */
  sage('blade')->compiler()->directive('posts', function ($query) {
      return '<?php while(have_posts()) : the_post(); ?>';
  });

  /**
   * Create @endposts Blade directive
   */
  sage('blade')->compiler()->directive('endposts', function () {
      return '<?php endwhile; ?>';
  });


  /**
   * Create @query() Blade directive
   */
  sage('blade')->compiler()->directive('query', function ($args) {
      $output = '<?php $bladeQuery = new WP_Query($args); ?>';
      $output .= '<?php while ($bladeQuery->have_posts()) : ?>';
      $output .= '<?php $bladeQuery->the_post(); ?>';

      return $output;
  });

  /**
   * Create @endquery Blade directive
   */
  sage('blade')->compiler()->directive('endquery', function () {
      $output = '<?php endwhile; ?>';
      $output .= '<?php wp_reset_query(); ?>';

      return $output;
  });

  /**
   * Create @title Blade directive
   */
  sage('blade')->compiler()->directive('title', function () {
      return '<?php the_title(); ?>';
  });

  /**
   * Create @subtitle Blade directive
   */
  sage('blade')->compiler()->directive('subtitle', function () {
      return '<?php echo get_post_meta(get_the_ID(), "_subtitle", true); ?>';
  });

  /**
   * Create @content Blade directive
   */
  sage('blade')->compiler()->directive('content', function () {
      return '<?php the_content(); ?>';
  });

  /**
   * Create @excerpt Blade directive
   */
  sage('blade')->compiler()->directive('excerpt', function () {
      return '<?php the_excerpt(); ?>';
  });

  /**
   * Create @featured_img Blade directive --
   * check if a featured image is big enough to use in single view
   */
  sage('blade')->compiler()->directive('featured_img', function () {
      return 'App\featured_image_check();';
  });

  /**
   * Create @tall_featured_img Blade directive --
   * check if a featured image is big enough to use in TALL single view
   */
  sage('blade')->compiler()->directive('tall_featured_img', function () {
      return 'App\tall_featured_image_check();';
  });

  /**
   * Create @featured_img_caption Blade directive --
   * return caption value from featured image if it exists
   */

  sage('blade')->compiler()->directive('featured_img_caption', function () {
      return '<?php App\get_featured_image_caption(); ?>';
  });

  /**
   * Create @column_title Blade directive --
   * return columnt title / link  if it exists
   */

  sage('blade')->compiler()->directive('column_title', function () {
      return 'App\get_column_title();';
  });

  /**
   * Create @author_bio_tag Blade directive --
   * return a brief description of an author if exists
   */

  sage('blade')->compiler()->directive('author_bio_tag', function () {
      return '<?php echo App\get_author_bio_tag(); ?>';
  });

  /**
   * Conditional date return
   * Add a year if > 6 months
   */

  sage('blade')->compiler()->directive('conditional_date', function () {
      return '<?php echo App\get_conditional_date(); ?>';
  });

  /**
   * Create @get_content_type Blade directive --
   * return a human-friendly description of our content type
   */

  sage('blade')->compiler()->directive('content_type', function () {
      return '<?php echo App\get_content_type(); ?>';
  });

  sage('blade')->compiler()->directive('issue_number', function () {
      return '<?php echo App\get_issue_number(); ?>';
  });

  sage('blade')->compiler()->directive('issue_slug', function () {
      return '<?php echo App\get_issue_slug(); ?>';
  });

});





/**
 * Init config
 */
sage()->bindIf('config', Config::class, true);
