<?php

namespace App;


add_filter( 'ssp_rss_stylesheet', 'App\\custom_ssp_rss_stylesheet' );

function custom_ssp_rss_stylesheet( ) {
	$stylesheet_url = asset_path('styles/podcasts.xsl');
	return $stylesheet_url;
}

// change the 'Podcast page' setting in SSP backend

add_filter( 'ssp_archive_slug', 'App\\custom_podcast_page_slug' );

function custom_podcast_page_slug( $slug ) {
	return 'bafflercasts';
};

// stop SSP from placing a <link> tag referencing the main podcast feed in the global head
add_filter( 'ssp_show_global_feed_tag', '__return_false');