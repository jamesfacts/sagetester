<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
        global $post;
        if ($post->post_parent == 127 || $post->ID == 127) {
            $classes[] = 'baffler-about';
        }
    }
    
    // figure out what type of magazine post we're in 

    if ( is_single() && get_post_type( get_the_ID() ) == 'article' ) {
        $s_content_type = wp_get_object_terms( get_the_ID(), 'content_type' )[0]->slug;
        $classes[] = $s_content_type;
        
        if ( in_array( $s_content_type, ['salvos', 'outbursts', 'odds-and-ends', 'ancestors', 'intros-and-manifestos']) ){
            $s_post_date = get_the_date( 'Y-m-d', get_the_ID() ); // how old is this post?
            $s_post_date_obj = strtotime( $s_post_date ); // make WP date an object            

            if ( $s_post_date_obj < strtotime('-18 months') ) {
                $classes[] = 'oldie';
            }
        }
                   
    }

    // Add class if sidebar is active
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    $classes[] = 'global';

    return $classes;
});


add_filter( 'open_graph_protocol_metas', 'App\\remove_caption_from_excerpt' );
function remove_caption_from_excerpt( $excerpt ) {
    return strip_shortcodes($excerpt);
}


function wpse_allowedtags() {
    // Add custom tags to this string
        return '<style>,<br>,<em>,<i>,<br/>'; 
    }


// https://wordpress.stackexchange.com/questions/141125/allow-html-in-excerpt

function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
$raw_excerpt = $wpse_excerpt;
    if ( '' == $wpse_excerpt ) {

        $wpse_excerpt = get_the_content('');
        $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
        $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
        $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
        $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

        //Set the excerpt word count and only break after sentence is complete.
            $excerpt_word_count = 20;
            $excerpt_char_count = 120;
            $excerpt_length = apply_filters('excerpt_length', 15 ); 
            $tokens = array();
            $excerptOutput = '';
            $count = 0;
            $char_count = 0;

            // Divide the string into tokens; HTML tags, or words, followed by any whitespace
            preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

            foreach ($tokens[0] as $token) { 

                if ($count >= $excerpt_length && preg_match('/[\;\?\.\!]\s*$/uS', $token)) { 
                // Limit reached, continue until ; ? . or ! occur at the end
                    $excerptOutput .= trim($token);
                    break;
                }

                // check to ensure our sentence doesn't exceed max character count
                if ($char_count >= $excerpt_char_count) {
                    $excerptOutput .= trim($token);
                    $excerptOutput .= ". . .";
                    break;
                }

                // Add words to complete sentence
                $count++;

                // Add characters to count
                $char_count += strlen($token);

                // Append what's left of the token
                $excerptOutput .= $token;
            }

        $wpse_excerpt = trim(force_balance_tags($excerptOutput));

        return $wpse_excerpt;   

    }
    return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
}



remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'App\\wpse_custom_wp_trim_excerpt'); 
add_filter('get_the_excerpt', function($text){
    return force_balance_tags($text);
});


/**
 * Here are some customizations that change text output via the gettext filter.
 * This was intended for translating themes to other languages, but why not
 * use it for more customization?
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 *
 */
add_filter( 'gettext', 'App\\change_excerpt_name', 20, 3 );
function change_excerpt_name( $translated_text, $text, $domain ) {

    // if( $_GET['post_type'] == 'events' ) {

        switch ( $translated_text ) {

            case 'Excerpt' :

                $translated_text = 'Homepage Summary';
                break;

            case 'Excerpts are optional hand-crafted summaries of your content that can be used in your theme. <a href="%s">Learn more about manual excerpts</a>.' :

                $translated_text = 'If you chose to override the programmatic summary, your excerpt will appear in full. Please keep to under 20 words / 120 characters!';
                break;

        }

    // }

    return $translated_text;
}



/**
 * Template Hierarchy should search for .blade.php files
 */
array_map(function ($type) {
    add_filter("{$type}_template_hierarchy", function ($templates) {
        return call_user_func_array('array_merge', array_map(function ($template) {
            $transforms = [
                '%^/?(templates)?/?%' => config('sage.disable_option_hack') ? 'templates/' : '',
                '%(\.blade)?(\.php)?$%' => ''
            ];
            $normalizedTemplate = preg_replace(array_keys($transforms), array_values($transforms), $template);
            return ["{$normalizedTemplate}.blade.php", "{$normalizedTemplate}.php"];
        }, $templates));
    });
}, [
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
]);

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = array_reduce(get_body_class(), function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    echo template($template, $data);

    // Return a blank file to make WordPress happy
    return get_theme_file_path('index.php');
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', 'App\\template_path');


/**
 *   Move the menu items around to my liking
 */

function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;

    remove_menu_page('edit-comments.php');
    remove_submenu_page('themes.php','theme-editor.php');
     
    return [
        'admin.php?page=wpengine-common', // WPEngine features
        'index.php', // Dashboard
        'separator1', // First separator
        'edit.php', // Word Factory posts
        'edit.php?post_type=article', // Contributors
        'edit.php?post_type=baffler_author', // Contributors
        'upload.php', // Media
        'edit.php?post_type=event', // Baffler events
        'edit.php?post_type=issue', // Issues
        'edit.php?post_type=page', // Pages
        'edit.php?post_type=book', // Baffler books
        'separator2', // Second separator
        'themes.php', // Appearance
        'plugins.php', // Plugins
        'users.php', // Users
        'tools.php', // Tools
        'options-general.php', // Settings
        'separator-last', // Last separator
    ];
}
add_filter('custom_menu_order', 'App\\custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'App\\custom_menu_order');


function na_remove_slug( $post_link, $post, $leavename ) {

    if ( 'article' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}

add_filter( 'post_type_link', 'App\\na_remove_slug', 10, 3 );


// add custom columns within Word Factory edit views

add_filter( 'manage_posts_columns', 'App\\add_wf_featured_columns');
add_action( 'manage_posts_custom_column' , 'App\\wf_custom_featured_columns', 10, 2 );


function add_wf_featured_columns( $columns ) {
    unset( $columns['author'], $columns['comments'] ); // remove WP user and comment count from admin area
    $column_meta = array( '_author' => 'Author', '_featured' => 'Feature on Homepage', '_top_rollout' => 'Top Rollout', '_editorpick' => 'Editor\'s Picks'/*, 'issue' => 'Issue' , '_editorpick' => 'Editor\'s Picks'*/);
    $columns = array_slice( $columns, 0, 2, true ) + $column_meta + array_slice( $columns, 2, NULL, true );
    return $columns;
}

function wf_custom_featured_columns( $column, $post_id ) {

    $content = '';

    switch ( $column ) {
    case '_author' :
            // get author
            $_author = get_post_meta($post_id, '_author');
            if(!empty($_author)) {
                $authors = $_author[0];
                if(is_array($authors)) {
                    $comma = '';
                    foreach($authors as $author => $value) {
                        $_author = get_post($value);
                        $content .= $comma . $_author->post_title;
                        $comma = ', ';
                    }
                } else {
                    $author = get_post($authors);
                    $content = $author->post_title;
                }
                echo $content;
            } else {
                echo '-';
            }
        break;
    case '_featured' :
            $featured = get_post_meta( $post_id , '_featured' , true ); 
            echo !empty($featured)?'Yes':' ';
        break;
    case '_top_rollout' :
            $top_rollout = get_post_meta( $post_id , '_top_rollout' , true ); 
            echo !empty($top_rollout)?'Yes':' ';
        break;
    case '_editorpick' :
        $editorpick = get_post_meta( $post_id , '_editorpick' , true );
        echo !empty($editorpick)?'Yes':' ';
        break;
    }
}

// add custom columns within Magazine edit views

// add_filter( 'manage_article_posts_columns', 'App\\add_article_new_columns');


// function add_article_new_columns( $columns ) {
//     $column_meta = array( '_author' => 'Author', '_featured' => 'Feature on Homepage', '_top_rollout' => 'Top Rollout', 'editorpick' => 'Editor\'s Picks'  );
//     $columns = array_slice( $columns, 0, 2, true ) + $column_meta + array_slice( $columns, 2, NULL, true );
//     return $columns;
// }

// function mag_custom_article_columns( $column, $post_id ) {
//     $editorpick = get_post_meta( $post_id , '_editorpick' , true );
//     echo !empty($editorpick)?'Yes':' ';
// }


// remove category base 

add_filter('category_rewrite_rules', 'App\\no_category_base_rewrite_rules');

function no_category_base_rewrite_rules($category_rewrite=array()) {
    
    $categories = get_categories(['hide_empty'=>false]);

    foreach($categories as $category) {
        $category_nicename = $category->slug;
        if ( $category->parent == $category->cat_ID )
            $category->parent = 0;
       elseif ($category->parent != 0 )
            $category_nicename = get_category_parents( $category->parent, false, '/', true ) . $category_nicename;
            $category_rewrite['('.$category_nicename.')/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?category_name=$matches[1]&feed=$matches[2]';
            $category_rewrite['('.$category_nicename.')/page/?([0-9]{1,})/?$'] = 'index.php?category_name=$matches[1]&paged=$matches[2]';
            $category_rewrite['('.$category_nicename.')/?$'] = 'index.php?category_name=$matches[1]';
    }

    global $wp_rewrite;
    $old_base = $wp_rewrite->get_category_permastruct();
    $new_base = str_replace( '%category%', '(.+)', $old_base );
    $new_base = trim($new_base, '/');
    $category_rewrite[$new_base.'$'] = 'index.php?category_redirect=$matches[1]';
    return $category_rewrite;
}

// TODO: remove this once we're happy with URLs

// flush_rewrite_rules();

/**
 * Add new rewrite rule for the blog sutff. 
 */
function createBafflerColumnUrls () {

    $categories = get_categories(['hide_empty'=>false]);

    foreach ($categories as $category) {
        $termValues = get_term_meta( $category->term_id, '_column' );
        $column = '';

        foreach ($termValues as $key => $value) {
            if ($value == '1') {
                $column = $category->slug;
            }
        }

        // $column     = ( get_term_meta( $category->term_id, '_column' )[0] == '1') ?  : '';
        if ( $column !== '' ) {
                $rewritePattern    = $column . '/([^/]*)$';
                $rewriteTag        = '%' . $column . '%';

                // var_dump($rewritePattern);
                // var_dump($rewriteTag);

                add_rewrite_rule(
                    $rewritePattern,
                    'index.php?name=$matches[1]',
                    'top'
                );
                add_rewrite_tag($rewriteTag,'([^/]*)');
        }
    }


}
add_action('init', 'App\\createBafflerColumnUrls', 999 );

/**
 * Modify post link
 * This will print /blog/post-name instead of /post-name
 */
function appendColumnQueryString ( $url, $post, $leavename ) {

        // Get the categories for the post
        $post             = get_post( get_the_ID() );
        $termValues         = get_the_category( get_the_ID() ); 
        $column = '';
        $slug = '';

        
        foreach ($termValues as $key => $term) {
            if ( get_term_meta( $term->term_id, '_column' ) ) {
                $column = $term->slug;
            }
        }

        if ( $column == '' ) { 
            return $url;
        }

        // If $url contains %postname% placeholder
        if ( false !== strpos( $url, '%postname%' ) ) {
         $slug = '%postname%';
        } elseif ( $post->post_name ) {
         $slug = $post->post_name;
        } else {
         $slug = sanitize_title( $post->post_title );
        }

        if ( $post->post_type == 'post' ) {     
            $url = home_url( user_trailingslashit( "$column/$slug" ) );
        }
    return $url;
}
add_filter( 'post_link', 'App\\appendColumnQueryString', 10, 3 );


add_filter( 'get_the_archive_title', function ( $title ) {
    if( is_category() ) {
        $title = single_cat_title( '', false );
    } 

    return $title;
});


/**
 * Filter posts preview links to point to the correct URL.
 * Important because we messed with the URLs for column posts inside the admin area
 *
 * @param  string  $preview_link The preview link URL.
 * @param  WP_Post $post         The post.
 * @return string  $url          The modified link URL.
 */
function filterColumnPreviewLink( $preview_link, $post ) {
   if ( $post->post_type === 'post' ) {
      $preview_link = add_query_arg( array(
         'p'       => $post->ID,
         'preview' => 'true',
      ), home_url( '/' ) );
   }
   if ( $post->post_type === 'article' ) {
      $preview_link = add_query_arg( array(
         'post_type'  => 'article',
         'p' => $post->ID,
      ), home_url( '/' ) );
   }
   return $preview_link;
}


add_filter( 'preview_post_link', 'App\\filterColumnPreviewLink', 10, 2 );


add_filter( 'the_author', 'App\\nbt_feed_author' );

function nbt_feed_author($article_authors) { 
    $authors = baffler_author_meta(get_the_ID(), false);
    return $authors;
}

// remove version info from head and feeds
add_filter('the_generator', function () {
    return '<generator>https://thebaffler.com</generator>';
});

add_filter('the_permalink_rss', 'App\\nbtRssCampaign');

function nbtRssCampaign($post_permalink) {
    // are we inside a podcast feed? 
    global $wp_query;
    
    $tracking = 'utm_source=rss-feed&utm_medium=rss&utm_campaign=';
    $tracking .= $wp_query->query['feed'] ? $wp_query->query['feed'] : 'danger-no-data';
    $tracking = urlencode($tracking);

    return $post_permalink . '?' . $tracking;
};


/* This title filter only works on The SEO Framework plugin, filters content after fetching $title 
   See: https://theseoframework.com/docs/api/filters/ */ 

add_filter( 'the_seo_framework_pro_add_title', 'App\\my_pre_title', 10, 3 );

function my_pre_title( $title = '', $args = array(), $escape = true ) {
    $title = wp_strip_all_tags($title);
    
    return $title;
}


    





