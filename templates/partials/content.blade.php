<article 
	@php
		global $wp_query;

		$current_post = $bladeQuery->current_post ? $bladeQuery->current_post : $wp_query->current_post;

		if ($current_post === 0 || $current_post % 2 == 0) {
			post_class(['col-10 offset-1 col-sm-12 offset-sm-0 col-md-5 offset-md-2']);
		} else {
			post_class(['col-10 offset-1 col-sm-12 offset-sm-0 col-md-5']);
		}
	@endphp
	>
	<section class="row">
		
		@if(App\get_content_type() == 'poems')
			<header class="meta col-12 col-sm-10">
				<h3 class="hed"><a href="{{ get_permalink() }}">@title</a></h3>
				<span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</span>
				<span class="date">@conditional_date</span>
			</header>
		@else	
			<div class="image-fill clickable col-12 col-sm-6" 
						 style="background-image: url({{ the_post_thumbnail_url('w500') }});">
				<a href="{{ the_permalink() }}" class="full-coverage hidden-sm-down" alt="{{ get_the_title() }}">&nbsp;</a>	
			</div>
			<header class="meta col-12 col-sm-6">
				<h3 class="hed"><a href="{{ get_permalink() }}">@title</a></h3>
					@php 
						$cat_id = get_query_var('cat');
						if ( get_term_meta( $cat_id, '_column' )[0] !== '1' )
							$column = App\get_column_title();
					@endphp

		  		@if($column)
						<div class="column-title"><a href="{{ home_url( '/' . $column->slug ) }}">{!! $column->name !!}</a></div>
					@endif
					<span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</span><span class="date">@conditional_date</span>
			</header>
		@endif

	</section> 
</article>
