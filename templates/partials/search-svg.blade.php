<svg 
	version="1.1"
	xmlns="http://www.w3.org/2000/svg" 
	xmlns:xlink="http://www.w3.org/1999/xlink" 
	x="0px" y="0px" 
	width="19.7px" height="19.7px" 
	viewBox="0 0 19.7 19.7" 
	style="enable-background:new 0 0 19.7 19.7;"
	xml:space="preserve" 
	class="nav-mag">

	<g>
		<path d="M7.2,2.2c-2.6,2.6-2.9,6.7-0.7,9.6L0,18.3l1.4,1.4L8,13.1c3.2,2.4,7.8,1.8,10.2-1.4S20,3.9,16.8,1.5
			C13.9-0.7,9.8-0.4,7.2,2.2L7.2,2.2z M16.2,11.1c-2.1,2.1-5.4,2.1-7.5,0s-2.1-5.4,0-7.5s5.4-2.1,7.5,0c1,1,1.6,2.3,1.6,3.7
			C17.7,8.7,17.1,10.1,16.2,11.1L16.2,11.1z"/>
	</g>

</svg>