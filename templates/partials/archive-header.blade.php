@php 
	$featured_img = App\featured_image_check();
	$tall_featured_img = App\tall_featured_image_check();
	$subtitle = get_post_meta(get_the_ID(), "_subtitle", true);
@endphp

@if ($featured_img)

<header class="header-contain">
  <div class="row meta-wrap">
		<div class="archive-contain">
	  	<div class="image-contain col-12 col-md-6">
	  		<div class="hero-image row">
	  			@if($tall_featured_img)
	  				<div class="image-fill tall col-12" style="background-image: url( {{ get_the_post_thumbnail_url(get_the_ID(), 'w700')}} );"></div> <!-- end image fill -->
		  			<div class="caption wf hidden-md-up col-12">@featured_img_caption</div>
		  		@else
						<div class="image-fill col-12" style="background-image: url( {{ get_the_post_thumbnail_url(get_the_ID(), 'w700')}} );"></div> <!-- end image fill -->
		  			<div class="caption wf hidden-md-up">@featured_img_caption</div>
		  		@endif

	  		</div> <!-- end image row -->
	  	</div>
	  	<div class="meta col-12 col-md-6 text-center">	
				<div class="stick">	  		
					<div class="column-title">From The Archive</div>
	  			<div class="author-meta">
	  				<span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</span>
						<div class="issue">
							<span class="number"><a href="{{ home_url('/issues/') . App\get_issue_slug() }}">@issue_number</a></span>
							<span class="date">&nbsp;{{ App\get_issue_date() }}</span>
						</div>
	  			</div>
	  		</div>
	  		<h1 class="entry-title">{!! get_the_title() !!}</h1>
				
				<h2 class="entry-subtitle">&nbsp;@subtitle&nbsp;</h2>
				
			</div>
		</div> <!-- end row with image -->
	</div> 
	<div class="row caption-wrap">
	  	<div class="caption wf hidden-sm-down col-6">@featured_img_caption</div>
  </div>
</header>
@else

<header class="header-contain salvo-fold archive-salvo">
  <div class="row meta-wrap">
			<div class="meta col-12 text-center no-img">
				<div class="stick">	  		
					<div class="column-title">From The Archive</div>
	  			<div class="author-meta">
	  				<span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</span>
						<div class="issue">
							<span class="number"><a href="{{ home_url('/issues/') . App\get_issue_slug() }}">@issue_number</a></span>
							<span class="date">&nbsp;{{ App\get_issue_date() }}</span>
						</div>
	  			</div>
	  		</div>
	  		<h1 class="entry-title">{!! get_the_title() !!}</h1>
				@if($subtitle)
					<h2 class="entry-subtitle">@subtitle</h2>
				@endif
			</div>

		</div> <!-- end row  -->
		
  
</header>

@endif


