

<section class="row page-wrap">
    @include('partials.page-header-about')
    <aside class="image-col col-12 col-sm-10 offset-sm-1 col-md-3 offset-md-1">
      @if(App\featured_image_check())
        <img src="{{the_post_thumbnail_url('w350')}}">
      @else
        <img src="{{ get_template_directory_uri() . '/dist/images/baffler-stack.png' }}">
      @endif
      <a href="{{ get_post_meta(get_the_ID(), '_buy_link', true) }}"><input type="submit" value="Buy" class="btn"></a>
    </aside>
    <article class="book-body col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0">
    @include('partials.content-page')
    </article>


  <article class="further-info">
    @php 
      $author_bio_tag = App\get_author_bio_tag();
    @endphp
    @if($author_bio_tag)
      <section class="row">
        <div class="col-12 offset-0 col-sm-10 offset-sm-1">
          @include('partials/author-bio')
        </div>
      </section>
    @endif
    <section class="icon-contain">
      <div class="col-12 offset-0 col-sm-10 offset-sm-1">
        @include('partials/share-icons')
      </div>
    </section>

    <section class="single-email row"> 
      @include('partials/emember-horz')
    </section>

  </article>
</section>