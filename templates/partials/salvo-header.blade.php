<header class="header-contain salvo-fold">
  <div class="row meta-wrap">
		<div class="meta col-12 col-md-8 text-center yes-img">
			<div class="author-meta">
				<span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</span>
				<span class="date"><a href="{{ home_url('/issues/') . App\get_issue_slug() }}">@issue_number</a></span>
			</div>
  		

  		<h1 class="entry-title">{!! get_the_title() !!}</h1>
			@if(@subtitle)
				<h2 class="entry-subtitle">@subtitle</h2>
			@endif
		</div> <!-- end meta -->


	</div>
	@php 
		$featured_img = App\featured_image_check();
	@endphp
	@if ($featured_img)

  	<div class="image-contain col-12 col-md-10 col-lg-8">
			<div class="hero-image row">
					<img class="salvo-featured" src="{{ get_the_post_thumbnail_url(get_the_ID()) }}" />			
	  			<div class="caption col-12">@featured_img_caption</div>
			</div>
		</div>
	@else 
		<div class="image-contain no-img col-12 col-md-8">
			
		</div>
	@endif



</header>




