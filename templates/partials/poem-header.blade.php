<header class="header-contain poem-fold">
  <div class="row meta-wrap">

		<div class="meta text-center no-img">
			
			<div class="author-meta">
				<span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</span>
				<span class="date"><a href="{{ home_url('/issues/') . App\get_issue_slug() }}">@issue_number</a></span>
			</div>
  		<div class="title-contain">
  			<h1 class="entry-title">{!! get_the_title() !!}</h1>
  		</div>

		</div> <!-- end meta -->

	</div>
</header>




