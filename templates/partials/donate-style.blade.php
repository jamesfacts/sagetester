<style type="text/css">
/* animations here */

@-webkit-keyframes animatedLeftHand {
	0% { left: -40%; }
	25% {	left: -40%;	}
	85% { left: -2%;}
	100% { left: -2%;	}
}

@keyframes animatedLeftHand {
	0% { left: -40%; }
	25% {	left: -40%;	}
	85% { left: -2%;}
	100% { left: -2%;	}
}

@-webkit-keyframes animatedDollar {
	0% { left: -40%; }
	8.33333333333333333% { left: -32%; }
	47% {	left: 23%; }
	57% {	left: 23%; }
	92% { left: 65%; }
	100% { left: 100%; }
}

@keyframes animatedDollar {
	0% { left: -40%; }
	8.33333333333333333% { left: -32%; }
	47% {	left: 23%; }
	57% {	left: 23%; }
	92% { left: 65%; }
	100% { left: 100%; }
}

@-webkit-keyframes animatedRightHand {
	0% { left: 100%;}
	50% {	left: 85%; }
	95% {	left: 58%; }
	100% { left: 58%;	}
}

@keyframes animatedRightHand {
	0% { left: 100%;}
	50% {	left: 85%; }
	95% {	left: 58%; }
	100% { left: 58%;	}
}

@-webkit-keyframes animatedBirdHand {
	0% { left: 88%; }
	30% { left: 88%; } 
	100% { left: 64%; }
}

@keyframes animatedBirdHand {
	0% { left: 88%; }
	30% { left: 88%; } 
	100% { left: 64%; }
}

@-webkit-keyframes animatedBird {
	0% {
		top: -100%;
		left: 50%;
	}
	41.666666666666666666% { 
		top: -100%;
		left: 50%;	
	}
	50% {
		top: -81%;
		left: 32%;
	}

	62% {
		top: -87%;
		left: 23%;
	}

	74% {
		top: -56%;
		left: 14%;
	}

	86% {
		top: -47%;
		left: 9%;
	}

	100% {
		top: -39%;
		left: 7%;	
	}
	
}

@keyframes animatedBird {
	0% {
		top: -100%;
		left: 50%;
	}
	41.666666666666666666% { 
		top: -100%;
		left: 50%;	
	}
	50% {
		top: -81%;
		left: 32%;
	}

	62% {
		top: -87%;
		left: 23%;
	}

	74% {
		top: -56%;
		left: 14%;
	}

	86% {
		top: -47%;
		left: 9%;
	}

	100% {
		top: -39%;
		left: 7%;	
	}
	
}


@-webkit-keyframes animatedDonate {
	0% {
		content: 'PLEASE';
		visibility: visible;
		top: -120%;
  	left: -32%;
	}
	49% {
		content: 'PLEASE';
		visibility: visible;
		top: -120%;
  	left: -32%;
	}
	50% {
		content: 'DONATE';
		visibility: visible;
		top: -109%;
    left: -26%;
	}
	100% {
		content: 'DONATE';
		visibility: visible;
		top: -109%;
    left: -26%;
	}
}


@keyframes animatedDonate {
	0% {
		content: 'PLEASE';
		visibility: visible;
		top: -120%;
  	left: -32%;
	}
	49% {
		content: 'PLEASE';
		visibility: visible;
		top: -120%;
  	left: -32%;
	}
	50% {
		content: 'DONATE';
		visibility: visible;
		top: -109%;
    left: -26%;
	}
	100% {
		content: 'DONATE';
		visibility: visible;
		top: -109%;
    left: -26%;
	}
}

@-webkit-keyframes animatedDonateTag {
	0% {
		visibility: visible;
		top: -120%;
  	left: -32%;
	}
	49% {
		visibility: visible;
		top: -120%;
  	left: -32%;
	}
	50% {
		visibility: visible;
		top: -109%;
    left: -26%;
	}
	100% {
		visibility: visible;
		top: -109%;
    left: -26%;
	}
}

@keyframes animatedDonateTag {
	0% {
		visibility: visible;
		top: -120%;
  	left: -32%;
	}
	49% {
		visibility: visible;
		top: -120%;
  	left: -32%;
	}
	50% {
		visibility: visible;
		top: -109%;
    left: -26%;
	}
	100% {
		visibility: visible;
		top: -109%;
    left: -26%;
	}
}

@-webkit-keyframes passTheBuck {
	0% {
		content: 'PASS';
		visbility: visible;
	}
	32% {
		content: 'PASS';
		visbility: visible;
	}
	33% {
		content: 'THE';
		visbility: visible;
	}
	65% {
		content: 'THE';
		visbility: visible;
	}
	66% {
		content: 'BUCK';
		visbility: visible;
	}
	100% {
		content: 'BUCK';
		visbility: visible;
	}
}

@keyframes passTheBuck {
	0% {
		content: 'PASS';
		visbility: visible;
	}
	32% {
		content: 'PASS';
		visbility: visible;
	}
	33% {
		content: 'THE';
		visbility: visible;
	}
	65% {
		content: 'THE';
		visbility: visible;
	}
	66% {
		content: 'BUCK';
		visbility: visible;
	}
	100% {
		content: 'BUCK';
		visbility: visible;
	}
}

</style>