<section class="row page-wrap">
  @include('partials/about-page-header')

  <article @php(post_class(["col-12 col-md-11 offset-md-1"]))>
    <section class="row entry-row">
      @if(App\featured_image_check())
        <aside class="col-12 col-md-3 bio-photo">
          <div class="image-fill" style="background-image: url( {{the_post_thumbnail_url('w350')}} );">
          </div>
        </aside>
        <div class="col-12 col-md-8 entry-content">
      @else
        <div class="col-12 col-md-8 offset-md-2 entry-content">
      @endif
        @php(the_content('', true))
      </div>
    </section>
  </article>
</section>

<section class="row author-stories">
@php
  $paged = ( get_query_var('work') ) ? get_query_var('work') : 1;
  
  $args = [
    'post_type' => ['post','article'],
    'post_status' => 'publish',
    'order' => 'DESC',
    'orderby' => 'post_date',
    'posts_per_page' => 8,
    'paged' => $paged,
    'meta_query' => [
      'relation' => 'OR',
      [
        'key' => '_author',
        'value' => '"' . get_the_ID() . '"',
        'compare' => 'LIKE'
      ], [
        'key' => '_author',
        'value' => get_the_ID(),
        'compare' => '='
      ]
    ]
  ];

@endphp

@query($args)
  @include ('partials.content-'.(get_post_type() !== 'post' ? App\get_content_type() : get_post_format()))
@endquery


@if ($bladeQuery->max_num_pages > 1) 
  <nav class="navigation posts-navigation" role="navigation">
    <h2 class="screen-reader-text">Posts navigation</h2>
    <div class="nav-links">
      @if($paged < $bladeQuery->max_num_pages)
      <div class="nav-previous"><a href="{{trailingslashit( get_post_permalink( get_the_ID() ) ) . 'work/' . ($paged + 1)}}">older work</a></div>
      @endif
      @if($paged == 2)
      <div class="nav-next"><a href="{{ post_permalink( get_the_ID() ) }}">newer work</a></div>
      @elseif($paged > 2)
      <div class="nav-next"><a href="{{trailingslashit( get_post_permalink( get_the_ID() ) ) . 'work/' . ($paged - 1)}}">newer work</a></div>
      @endif
    </div>
  </nav>
@endif



</section>


<section class="further-info">

  <section class="icon-contain">
    <div class="col-12 offset-0 col-sm-10 offset-sm-1">
      @include('partials/share-icons')
    </div>
  </section>

  <section class="single-email row"> 
    @include('partials/emember-horz')
  </section>

</section>



