@php
  $args = ['numberposts' => '1',
					 'posts_per_page' => '1',
					 'post_status' => 'publish',
					 'post_type' => ['article', 'post'],
					 'meta_query' => [['key' => '_big_story', 'value' => 1]],
 					 'orderby'     => 'modified',
     			 'order'       => 'DESC'
					 ];
@endphp

@query($args)
	<section class="row big-story">
		<div class="image-fill col-md-6 lede-image"
			style="background-image: url( {{the_post_thumbnail_url('w700')}} );">
			<a href="{{ the_permalink() }}" class="full-coverage">
				&nbsp;
			</a>
		</div>
		<article class="text-bucket col-md-6">
			<h2 class="author">
				{!! App\baffler_author_meta(get_the_ID(), true, true) !!}
			</h2>
			<h1 class="hed">
				<a href="{{ the_permalink() }}">@title</a>
			</h1>
			<h3 class="subhed">
				<a href="{{ the_permalink() }}">@subtitle</a>
			</h3>
		</article>
	</section>
	@php $displayed[] = get_the_ID()
	@endphp
@endquery


@php
  $args = ['post__not_in' => $displayed,
					 'posts_per_page' => '3',
					 'post_status' => 'publish',
					 'post_type' => ['article', 'post'],
					 'meta_query' => [['key' => '_top_rollout', 'value' => 1]],
					 'orderby'     => 'modified',
     			 'order'       => 'DESC'
					 ];
@endphp

<section class="row top-rollout">
@query($args)

	<article class="row col-12 col-md-6 col-lg-4 story">

		<div class="col-6 image-fill"
		style="background-image: url( {{the_post_thumbnail_url('w350')}} );">
			<a href="{{ the_permalink() }}" class="full-coverage">
				&nbsp;
			</a>
		</div>

		<div class="col-6 text-bucket">
			<h1 class="hed">
				<a href="{{ the_permalink() }}">@title</a>
			</h1>
			<h2 class="author">
				{!! App\baffler_author_meta(get_the_ID(), true, true) !!}
			</h2>
		</div>

	</article>

  @php $displayed[] = get_the_ID()
	@endphp

@endquery

</section>



<section class="row home-emember">
@include('partials.emember-horz')
</section>


<section class="row columns-n-mag">

 <div class="col-md-6 offset-md-0 col-sm-8 offset-sm-2 col-10 offset-1 column-box">
	@php
	$all_columns = App\get_all_columns();

  $args = ['post__not_in' => $displayed,
					 'posts_per_page' => '3',
					 'post_status' => 'publish',
					 'post_type' => ['post'],
					 'meta_query' => [['key' => '_featured', 'value' => 1]],
					 'tax_query' => [['taxonomy' => 'category',	'field' => 'slug', 'terms' => $all_columns]],
					 'orderby'     => 'modified',
     			 'order'       => 'DESC'
					 ];

	@endphp
	@query($args)
		<article>
			@php $cats = get_the_category(get_the_ID())
			@endphp

			@foreach($cats as $cat)
					@if ( in_array( $cat->slug, $all_columns)	)
					<h4 class="title"><a href="{{ home_url( '/' . $cat->slug ) }}">{{$cat->name}}</a></h4>
					@endif
			@endforeach

			<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>

			<h5 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h5>
		</article>
	  @php $displayed[] = get_the_ID()
		@endphp
		<hr class="split">

	@endquery


 </div>




	 <div class="col-sm-12 col-md-6 mag-box">
	 	<div class="shading">
		 	@php
		 		$args = ['post_status' => 'publish',
								 'post_type' => ['issue'],
								 'posts_per_page' => '-1'
								 ];
			  $allIssues = new WP_Query($args);
				$issueNo = intval($allIssues->post_count);
	 		@endphp
	 		<h1 class="text-center title">From the Magazine</h1>
		 	<h6 class="text-center issue">Current issue, no. {{$issueNo}}</h6>
		 	<div class="row story-wrap">
		 		<div class="col-md-6 offset-md-0 col-sm-8 offset-sm-2 col-10 offset-1 copy">
		 			@php
					  $args = ['post__not_in' => $displayed,
										 'posts_per_page' => '3',
										 'post_status' => 'publish',
										 'post_type' => ['article'],
										 'meta_query' => [['key' => '_editorpick', 'value' => 1]],
										 //'orderby'     => 'modified',
					     			 'order'       => 'DESC'
										 ];
					@endphp
					@query($args)
				 	<article>
				 		<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
				 		<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
				 		<h5 class="subhed">@subtitle</h5>
			 		</article>
		 			<hr class="split">
	 				  @php $displayed[] = get_the_ID()
						@endphp
					@endquery
		 		</div>
		 		<div class="col-md-6 offset-md-0 col-sm-8 offset-sm-2 col-10 offset-1 cover-wrap">
		 			<a href="{{ home_url( '/current-issue' ) }}">
				 			@php
							  $args = ['posts_per_page' => '1',
												 'post_status' => 'publish',
												 'post_type' => ['issue'],
												 'orderby'     => 'post_date',
							     			 'order'       => 'DESC'
												 ];
							@endphp
							@query($args)
								@php($fullCover = get_post_meta(get_the_ID(), "_issue_full_cover", true))
								@if($fullCover)
									<img class="cover" src="{!!$fullCover!!}" alt="@title — {{get_post_meta(get_the_ID(), "_issue_title", true)}}">
								@else
									<img class="cover" src="{{ the_post_thumbnail_url('w450') }}" alt="@title — {{get_post_meta(get_the_ID(), "_issue_title", true)}}">
								@endif
							@endquery
					</a>
				</div>
			</div>

			<div class="row sub-calls">

				<div class="col-md-6 offset-md-0 col-sm-8 offset-sm-2 col-10 offset-1">
					<a href="{{ home_url( '/subscribe' ) }}"><input type="submit" value="Subscribe" id="sub" class="btn"></a>
				</div>
				<div class="col-md-6 offset-md-0 col-sm-8 offset-sm-2 col-10 offset-1">
					<a href="{{ home_url( '/current-issue' ) }}"><input type="submit" value="Buy" class="btn"></a>
				</div>

			</div>
		</div>
	</div>

</section>

<section class="row low-rollout hidden-sm-down">

	@php
	  $args = ['post__not_in' => $displayed,
						 'posts_per_page' => '1',
						 'post_status' => 'publish',
						 'post_type' => ['article', 'post'],
						 'meta_query' => [['key' => '_top_rollout', 'value' => 1]],
						 'orderby'     => 'modified',
	     			 'order'       => 'DESC'
						 ];
	@endphp

	@query($args)
	<article class="col-xl-4 offset-xl-1 col-md-5 wide-image">
		<div class="image-fill clickable"
				 style="background-image: url({{ the_post_thumbnail_url('w500') }});">
			<a href="{{ the_permalink() }}" class="full-coverage" alt="@title">&nbsp;</a>
		</div>


		<div class="reveal text-center">
			<h3 class="hed"><a href="{{ the_permalink() }}">@title</a></h3>
			<span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!},</span><span class="date">&nbsp;{{ get_the_date('F j') }}</span>
		</div>
	</article>
		@php $displayed[] = get_the_ID()
		@endphp
	@endquery

	<div class="col-xl-6 col-md-7 with-summary">
		@php
		  $args = ['post__not_in' => $displayed,
							 'posts_per_page' => '2',
							 'post_status' => 'publish',
							 'post_type' => ['article', 'post'],
							 'meta_query' => [['key' => '_top_rollout', 'value' => 1]],
							 'orderby'     => 'modified',
		     			 'order'       => 'DESC'
							 ];
		@endphp
		@query($args)
			<article class="row home-card">
				<div class="col-xl-6 col-md-5 image-fill clickable"
				style="background-image: url({{ the_post_thumbnail_url('w350') }});">
					<a href="{{ the_permalink() }}" class="full-coverage"></a>
				</div>
				<div class="meta col-xl-6 col-md-7">
					<div>
						<h3 class="hed"><a href="{{ the_permalink() }}">@title</a></h3>
						<h5 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h5>
					</div>
					<p class="excerpt">{!!  get_the_excerpt( get_the_ID() ) !!}</p>
				</div>
			</article>
			@php $displayed[] = get_the_ID()
			@endphp
		@endquery


	</div>

</section>


<section class="row four-topic-wrap hidden-sm-down">
	<div class="row">
		<div class="summary-wrap latest col-xl-4 offset-xl-2 col-md-5 offset-md-1 ">
			<h1 class="cap">Latest</h1>
			<div class="inner-wrap">
				@php
				  $args = ['post__not_in' => $displayed,
									 'posts_per_page' => '2',
									 'post_status' => 'publish',
									 'post_type' => ['article', 'post'],
									 'tax_query' => [
									 		['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => ['stories', 'poems'], 'operator' => 'NOT IN']],
									 'meta_query' => [['key' => '_featured', 'value' => 1]],
									 ];
				@endphp
				@query($args)
				<article class="home-card">
					<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
					<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
					<p class="excerpt">{!! get_the_excerpt( get_the_ID() ) !!}</p>
				</article>
				<hr class="split">
					@php $displayed[] = get_the_ID();
					@endphp
				@endquery
			</div>
			<div class="read-on">
				<a href="{{ home_url( '/latest' ) }}"><input type="submit" class="btn" value="More Latest"></a>
			</div>
		</div>

		<div class="summary-wrap fiction col-xl-4 col-md-5">
			<h1 class="cap">Fiction</h1>
				<div class="inner-wrap">
					@php
					  $args = ['post__not_in' => $displayed,
										 'posts_per_page' => '2',
										 'post_status' => 'publish',
										 'post_type' => 'article',
										 'tax_query' => [
										 		['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => 'stories' ] ],
										 'meta_query' => [['key' => '_featured', 'value' => 1]],
										 ];
					@endphp
					@query($args)
					<article class="home-card">
						<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
						<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
						<p class="excerpt">{!! get_the_excerpt( get_the_ID() ) !!}</p>
					</article>
					<hr class="split">
					@endquery
				</div>
			<div class="read-on">
				<a href="{{ home_url( '/stories' ) }}"><input type="submit" class="btn" value="More Fiction"></a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="summary-wrap poetry col-xl-4 offset-xl-2 col-md-5 offset-md-1">
			<h1 class="cap">
				<svg version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
					 x="0px" y="0px" width="100px" height="23.9px" viewBox="0 0 95.6 22.9" style="enable-background:new 0 0 95.6 22.9;"
					 xml:space="preserve">
				<defs>
				</defs>
				<g>
					<path d="M0,0h6.7c4.7,0,7.9,2.9,7.9,7.2c0,4.2-3.2,7.2-7.9,7.2h-3v8.2H0V0z M6.6,10.9c2.7,0,4.3-1.5,4.3-3.7S9.3,3.4,6.6,3.4H3.7
						v7.5H6.6z"/>
					<path d="M16.5,14.6c0-4.6,3.5-8.2,8.2-8.2s8.2,3.6,8.2,8.2s-3.5,8.2-8.2,8.2S16.5,19.2,16.5,14.6z M29.4,14.6
						c0-2.8-1.9-4.9-4.6-4.9s-4.6,2.1-4.6,4.9s1.9,4.9,4.6,4.9C27.5,19.4,29.4,17.3,29.4,14.6z"/>
					<path d="M36.5,14.6c0-4.6,3.5-8.2,8.1-8.2c4.4,0,7.4,3.1,7.6,8.2v1H40.1c0.3,2.3,2.2,3.8,4.6,3.8c1.9,0,3.3-0.9,4.1-1.8l2.3,2.5
						c-1.4,1.6-3.7,2.8-6.5,2.8C40,22.8,36.5,19.2,36.5,14.6z M40.2,12.8h8.1c-0.4-2.2-1.9-3.2-4-3.2C42.5,9.6,40.8,10.8,40.2,12.8z"/>
					<path d="M77.1,13.3c0-1.7-1-3.6-3.3-3.6c-1.6,0-2.8,0.9-3.5,2.8c0,0.3,0,0.5,0,0.7v9.3h-3.5v-9.2c0-1.7-1-3.6-3.3-3.6
						c-1.5,0-2.8,0.9-3.4,2.7v10.1h-3.5V6.6h3.5v1.5c1.2-1.2,2.7-1.7,4.2-1.7c2.2,0,3.9,0.9,4.9,2.4c1.5-2,3.7-2.4,5.2-2.4
						c3.9,0,6.1,2.7,6.1,6.8v9.3H77L77.1,13.3L77.1,13.3z"/>
					<path d="M84.1,19.5l2.7-1.9c0.8,1.3,1.7,2,3.2,2c1.2,0,2-0.6,2-1.5s-0.7-1.5-2.1-2l-1.1-0.4c-2.7-1-3.9-2.7-3.9-4.9
						c0-2.5,2.2-4.4,5.1-4.4c2,0,3.8,0.8,4.9,2.8L92.3,11C91.8,10,91,9.6,90,9.6c-0.9,0-1.7,0.5-1.7,1.3s0.5,1.3,2,1.9l1.1,0.4
						c2.7,1,4.2,2.4,4.2,4.9c0,2.8-2.3,4.7-5.7,4.7C87.5,22.8,85.4,21.7,84.1,19.5z"/>
				</g>
				</svg>
			</h1>
			<div class="inner-wrap">
				@php
				  $args = ['post__not_in' => $displayed,
									 'posts_per_page' => '2',
									 'post_status' => 'publish',
									 'post_type' => 'article',
									 'tax_query' => [
									 		['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => 'poems' ] ],
									 'meta_query' => [['key' => '_featured', 'value' => 1]],
									 ];
				@endphp
				@query($args)
				<article class="home-card">
					<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
						<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
						<p class="excerpt">{!!  get_the_excerpt( get_the_ID() ) !!}</p>
				</article>
				<hr class="split">
				@endquery
			</div>
			<div class="read-on">
				<a href="{{ home_url( '/poems' ) }}"><input type="submit" class="btn" value="More Poetry"></a>
			</div>

		</div>

		<div class="summary-wrap more col-xl-4 col-md-5">
			<h1 class="cap">Word Factory</h1>
			<div class="inner-wrap">
				@php
				  $args = ['post__not_in' => $displayed,
									 'posts_per_page' => '2',
									 'post_status' => 'publish',
									 'post_type' => ['article', 'post'],
									 'tax_query' => [
									 		['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => ['stories', 'poems'], 'operator' => 'NOT IN']],
									 'meta_query' => [['key' => '_featured', 'value' => 1]],
									 ];
				@endphp
				@query($args)
				<article class="home-card">
					<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
					<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
					<p class="excerpt">{!!  get_the_excerpt( get_the_ID() ) !!}</p>
				</article>
				<hr class="split">
				@php $displayed = array_slice($displayed, 0, 13)
				@endphp
				@endquery
			</div>
			<div class="read-on">
				<a href="{{ home_url( '/latest' ) }}"><input type="submit" class="btn" value="More Word Factory"></a>
			</div>
		</div>
	</div>
</section>


<section class="row four-topic-wrap hidden-md-up">

  <div id="fourTopicCarousel" class="carousel slide" data-ride="false" data-interval="false">
    <div class="carousel-inner" role="listbox">

      <div class="row carousel-item active">

          <div class="latest col-sm-8 offset-sm-2 col-10 offset-1">
            <h1 class="cap">Latest</h1>
           			<div class="inner-wrap">
									@php
									  $args = ['post__not_in' => $displayed,
														 'posts_per_page' => '2',
														 'post_status' => 'publish',
														 'post_type' => ['article', 'post'],
														 'tax_query' => [
														 		['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => ['stories', 'poems'], 'operator' => 'NOT IN']],
														 'meta_query' => [['key' => '_featured', 'value' => 1]],
														 ];
									@endphp
									@query($args)
									<article class="home-card">
										<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
										<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
										<p class="excerpt">{!! get_the_excerpt( get_the_ID() ) !!}</p>
									</article>
									<hr class="split">
										@php $displayed[] = get_the_ID()
										@endphp
									@endquery
								</div>
            <div class="read-on">
              <a href="{{ home_url( '/latest' ) }}"><input type="submit" class="btn" value="More Latest"></a>
            </div>

          </div>

      </div>

      <div class="row carousel-item">
        <div class="fiction col-sm-8 offset-sm-2 col-10 offset-1">
          <h1 class="cap">Fiction</h1>
          <div class="inner-wrap">
						@php
						  $args = ['post__not_in' => $displayed,
											 'posts_per_page' => '2',
											 'post_status' => 'publish',
											 'post_type' => 'article',
											 'tax_query' => [
											 		['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => 'stories' ] ],
											 'meta_query' => [['key' => '_featured', 'value' => 1]],
											 ];
						@endphp
						@query($args)
						<article class="home-card">
							<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
							<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
							<p class="excerpt">{!! get_the_excerpt( get_the_ID() ) !!}</p>
						</article>
						<hr class="split">
						@endquery
					</div>
          <div class="read-on">
            <a href="{{ home_url( '/stories' ) }}"><input type="submit" class="btn" value="More Fiction"></a>
          </div>
        </div>
      </div>

      <div class="row carousel-item">
        <div class="poetry col-sm-8 offset-sm-2 col-10 offset-1">
          <h1 class="cap">
            <svg version="1.1"
               xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
               x="0px" y="0px" width="100px" height="23.9px" viewBox="0 0 95.6 22.9" style="enable-background:new 0 0 95.6 22.9;"
               xml:space="preserve">
            <g>
              <path d="M0,0h6.7c4.7,0,7.9,2.9,7.9,7.2c0,4.2-3.2,7.2-7.9,7.2h-3v8.2H0V0z M6.6,10.9c2.7,0,4.3-1.5,4.3-3.7S9.3,3.4,6.6,3.4H3.7
                v7.5H6.6z"/>
              <path d="M16.5,14.6c0-4.6,3.5-8.2,8.2-8.2s8.2,3.6,8.2,8.2s-3.5,8.2-8.2,8.2S16.5,19.2,16.5,14.6z M29.4,14.6
                c0-2.8-1.9-4.9-4.6-4.9s-4.6,2.1-4.6,4.9s1.9,4.9,4.6,4.9C27.5,19.4,29.4,17.3,29.4,14.6z"/>
              <path d="M36.5,14.6c0-4.6,3.5-8.2,8.1-8.2c4.4,0,7.4,3.1,7.6,8.2v1H40.1c0.3,2.3,2.2,3.8,4.6,3.8c1.9,0,3.3-0.9,4.1-1.8l2.3,2.5
                c-1.4,1.6-3.7,2.8-6.5,2.8C40,22.8,36.5,19.2,36.5,14.6z M40.2,12.8h8.1c-0.4-2.2-1.9-3.2-4-3.2C42.5,9.6,40.8,10.8,40.2,12.8z"/>
              <path d="M77.1,13.3c0-1.7-1-3.6-3.3-3.6c-1.6,0-2.8,0.9-3.5,2.8c0,0.3,0,0.5,0,0.7v9.3h-3.5v-9.2c0-1.7-1-3.6-3.3-3.6
                c-1.5,0-2.8,0.9-3.4,2.7v10.1h-3.5V6.6h3.5v1.5c1.2-1.2,2.7-1.7,4.2-1.7c2.2,0,3.9,0.9,4.9,2.4c1.5-2,3.7-2.4,5.2-2.4
                c3.9,0,6.1,2.7,6.1,6.8v9.3H77L77.1,13.3L77.1,13.3z"/>
              <path d="M84.1,19.5l2.7-1.9c0.8,1.3,1.7,2,3.2,2c1.2,0,2-0.6,2-1.5s-0.7-1.5-2.1-2l-1.1-0.4c-2.7-1-3.9-2.7-3.9-4.9
                c0-2.5,2.2-4.4,5.1-4.4c2,0,3.8,0.8,4.9,2.8L92.3,11C91.8,10,91,9.6,90,9.6c-0.9,0-1.7,0.5-1.7,1.3s0.5,1.3,2,1.9l1.1,0.4
                c2.7,1,4.2,2.4,4.2,4.9c0,2.8-2.3,4.7-5.7,4.7C87.5,22.8,85.4,21.7,84.1,19.5z"/>
            </g>
            </svg>
          </h1>
          <div class="inner-wrap">
						@php
						  $args = ['post__not_in' => $displayed,
											 'posts_per_page' => '2',
											 'post_status' => 'publish',
											 'post_type' => 'article',
											 'tax_query' => [
											 		['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => 'poems' ] ],
											 'meta_query' => [['key' => '_featured', 'value' => 1]],
											 ];
						@endphp
						@query($args)
						<article class="home-card">
							<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
								<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
								<p class="excerpt">{!!  get_the_excerpt( get_the_ID() ) !!}</p>
						</article>
						<hr class="split">
						@endquery
					</div>
          <div class="read-on">
            <a href="{{ home_url( '/poems' ) }}"><input type="submit" class="btn" value="More Poetry"></a>
          </div>
        </div>
      </div>


      <div class="row carousel-item">
        <div class="more col-sm-8 offset-sm-2 col-10 offset-1">
          <h1 class="cap">Word Factory</h1>
          <div class="inner-wrap">
						@php
						  $args = ['post__not_in' => $displayed,
											 'posts_per_page' => '2',
											 'post_status' => 'publish',
											 'post_type' => ['article', 'post'],
											 'tax_query' => [
											 		['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => ['stories', 'poems'], 'operator' => 'NOT IN']],
											 'meta_query' => [['key' => '_featured', 'value' => 1]],
											 ];
						@endphp
						@query($args)
						<article class="home-card">
							<h2 class="hed"><a href="{{ the_permalink() }}">@title</a></h2>
							<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
							<p class="excerpt">{!!  get_the_excerpt( get_the_ID() ) !!}</p>
						</article>
						<hr class="split">
						@endquery
					</div>
          <div class="read-on">
            <a href="{{ home_url( '/latest' ) }}"><input type="submit" class="btn" value="More Word Factory"></a>
          </div>
        </div>
      </div>

    </div>
    <a class="carousel-control-prev" href="#fourTopicCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#fourTopicCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</section> <a class="carousel-control-prev" href="#fourTopicCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#fourTopicCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</section>
