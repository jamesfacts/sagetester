<div class="page-header col-12 col-md-11 offset-md-1">

	<h1 class="heading">Search results:&nbsp;{{get_search_query()}}</h1>
	<hr class="split">
	<h5 class="subhed">@php global $wp_query; echo $wp_query->found_posts; @endphp&nbsp;results found</h5>
	
</div>
<section class="search-wide-body col-12 col-md-11 offset-md-1">
	<form role="search" method="get" class="search-md" action="{{ home_url( '/' ) }}">
	  <label class="inline-search">
	    <span class="screen-reader-text">Search for:</span>
	    <input type="search" class="search-field" placeholder="Search again. . ." value="" name="s">
			<p class="hidden-md-down">Press return to see results</p>
	  </label>
	</form>
</section>
