@if (App\get_content_type() == 'book')
	<div class="page-header col-12 col-md-6 offset-md-4">
@else
	<div class="page-header col-12 col-md-6 offset-md-3">
@endif
	<h1 class="heading">@title</h1>
	<hr class="split">
</div>
