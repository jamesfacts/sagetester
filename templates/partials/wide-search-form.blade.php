
<!-- todo: pass a variable for the search placeholder here -->

<section class="search-wide-body col-12 col-md-11 offset-md-1">
	<form role="search" method="get" class="search-md" action="{{ home_url( '/' ) }}">
	  <label class="inline-search">
	    <span class="screen-reader-text">Search for:</span>
	    <input type="search" class="search-field" placeholder="Search again. . ." value="" name="s">
			<p class="hidden-md-down">Press return to see results</p>
	  </label>
	</form>
</section>