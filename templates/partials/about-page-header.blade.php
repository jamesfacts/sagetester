
@if(App\featured_image_check())
	<div class="page-header col-12 col-md-8 offset-md-3">
@else 
	<div class="page-header col-12 col-md-10 offset-md-2">
@endif
	<h1 class="heading">@title</h1>
	<hr class="split">
	
</div>

