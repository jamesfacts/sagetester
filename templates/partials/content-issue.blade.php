<article 
	@php
		if (get_query_var('paged') == 0 ) {
			global $wp_query; 
			$wp_query->current_post > 2 ? post_class(['col-10 offset-1 col-md-3 offset-md-0']) : post_class(['col-12 col-md-4']);
		} else {
			post_class(['col-10 offset-1 col-md-3 offset-md-0']);
		}
	@endphp
	>

	<div class="image-fill clickable" 
				 style="background-image: url({{ the_post_thumbnail_url('w500') }});">
		<a href="{{ the_permalink() }}" class="full-coverage hidden-sm-down" alt="{{ get_the_title() }}">&nbsp;</a>	
	</div>
	<header class="reveal text-center">
		<h3 class="hed"><a href="{{ get_permalink() }}">{{ get_the_title() }}—{!! get_post_meta(get_the_ID(), '_issue_title', true) !!}</a></h3>
		<span class="date">&nbsp;{{ get_the_date('F Y') }}</span>
	</header> 

</article>
