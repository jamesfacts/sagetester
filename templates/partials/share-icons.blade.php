@php $description = get_the_excerpt();
$description = wp_strip_all_tags($description)
@endphp

<div class="icon-wrap row">
	<div class="sharing-icon  facebook">
		<a target="_blank" class="inner-link" href="https://www.facebook.com/sharer/sharer.php?u={{ the_permalink() }}">
			<svg version="1.1"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
				 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">

				<g>
					<path d="M0,17.5C0,27.2,7.8,35,17.5,35S35,27.2,35,17.5S27.2,0,17.5,0S0,7.8,0,17.5z M12.6,14.1h2v-2.1
						c0-2.8,1.2-4.4,4.4-4.4h2.7V11h-1.7c-1.3,0-1.4,0.5-1.4,1.4v1.7h3.1l-0.4,3.4h-2.7v9.9h-4.1v-9.9h-2L12.6,14.1z"/>
				</g>
			</svg>
			<label>Share</label>
		</a>
	</div>
	<div class="sharing-icon  twitter">

		<a target="_blank" class="inner-link" data-via="thebafflermag" data-related="thebafflermag" href="https://twitter.com/intent/tweet?text=@title&url={{ the_permalink() }}">
			<svg version="1.1"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
				 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
				<g>
					<path d="M17.5,35C27.2,35,35,27.2,35,17.5S27.2,0,17.5,0S0,7.8,0,17.5S7.8,35,17.5,35z M10.9,20.2
						c0.5,0.1,1.1,0.1,1.6-0.1c-1.7-0.4-3-1.9-3-3.7c0.3,0.1,0.6,0.2,0.8,0.3c0.3,0.1,0.6,0.1,0.8,0.1c-0.8-0.6-1.3-1.4-1.5-2.3
						c-0.2-0.9-0.1-1.9,0.4-2.7c1.9,2.3,4.7,3.8,7.7,3.9c0-0.2,0-0.3-0.1-0.5c-0.2-2.1,1.3-3.9,3.4-4.1c0.3,0,0.6,0,1,0
						c0.8,0.1,1.5,0.5,2,1c0.1,0.1,0.1,0.1,0.2,0.1c0.7-0.2,1.4-0.4,2-0.8l0.2-0.1c-0.1,0.4-0.3,0.8-0.6,1.1c-0.3,0.3-0.6,0.6-0.9,0.9
						v0.1c0.3-0.1,0.7-0.1,1-0.2s0.7-0.2,1-0.3c-0.3,0.4-0.6,0.8-1,1.2c-0.2,0.2-0.5,0.4-0.8,0.7c-0.1,0.1-0.1,0.1-0.1,0.2
						c0,1.1-0.1,2.2-0.4,3.2c-0.2,0.9-0.6,1.7-1,2.5c-0.4,0.7-0.8,1.3-1.3,1.8c-0.6,0.7-1.4,1.4-2.2,1.9c-0.7,0.4-1.4,0.8-2.2,1.1
						c-1.3,0.4-2.6,0.6-4,0.5c-1.1,0-2.2-0.3-3.3-0.7c-0.7-0.3-1.3-0.6-1.9-1c1.9,0.2,3.9-0.4,5.4-1.6C12.8,22.6,11.5,21.6,10.9,20.2z"
						/>
				</g>
			</svg>
			<label>Tweet</label>
		</a>
	</div>
	<div class="sharing-icon  mail">
		<a class="inner-link" href="mailto:yourfriends@newbadthings.com?subject=@title&body={{$description}}" >
		<svg version="1.1"
			 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
			 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">

			<g>
				<path d="M17.5,35C27.2,35,35,27.2,35,17.5S27.2,0,17.5,0S0,7.8,0,17.5S7.8,35,17.5,35z M25.8,11.5l-8.2,5.9l-7.9-5.9
					H25.8z M8.6,12.8l8.4,6.3c0.3,0.2,0.7,0.2,1,0l9-6.4v10.6H8.6V12.8z"/>
			</g>
		</svg>
		<label>Email</label>
		</a>
	</div>
	<div class="sharing-icon  read">
		<a class="inner-link" id="read-drop-down" href="javascript:void(0)">
			<svg version="1.1"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
				 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
			<g>
				<path d="M17.5,0C7.8,0,0,7.8,0,17.5S7.8,35,17.5,35S35,27.2,35,17.5S27.2,0,17.5,0z M16.7,24.9
					c-2.3-1.6-5.1-2.5-8-2.5V10.9c2.9,0,5.7,0.9,8,2.5V24.9z M26.3,22.4c-2.9,0-5.7,0.9-8,2.5V13.5c2.3-1.6,5.1-2.5,8-2.5V22.4z"/>
				</g>
			</svg>
			<label>Read Later</label>
		</a>
	</div>


</div>

<div class="read-later not-displayed row">
	<div class="pocket reading-item">
		<a target="_blank" class="inner-link"  href="https://getpocket.com/edit?url={{ the_permalink() }}">
			<svg version="1.1"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
				 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
				<g>
					<path class="st0" d="M17.5,35C27.2,35,35,27.2,35,17.5S27.2,0,17.5,0S0,7.8,0,17.5S7.8,35,17.5,35z M10.4,11.4h14.3
						c0.7,0,1.4,0.5,1.5,1.2c0,0.1,0,0.2,0,0.3c0,1.9,0,3.7,0,5.6c0,1.9-0.6,3.7-1.8,5.1c-2.9,3.9-8.4,4.6-12.3,1.7
						c-1.5-1.2-2.6-2.8-3.1-4.7c-0.2-0.6-0.2-1.2-0.3-1.7c0-2,0-4,0-5.9C8.8,12.1,9.5,11.4,10.4,11.4z M12.6,18
						c-0.5-0.5-0.4-1.2,0.1-1.7c0.5-0.4,1.2-0.4,1.6,0l3.2,3l0.1,0.1L19,18l1.8-1.7c0.5-0.4,1.2-0.4,1.7,0.1c0.2,0.2,0.3,0.4,0.3,0.7
						c0,0.4-0.1,0.8-0.4,1l-3.5,3.4L18.3,22c-0.5,0.4-1.2,0.4-1.6,0L12.6,18z"/>
				</g>
			</svg>
			<label for="Pocket">
				Pocket
			</label>
		</a>
	</div>

	<div class="instapaper reading-item">

		<a target="_blank" class="inner-link" href="http://www.instapaper.com/hello2?url={{ the_permalink() }}&title=@title&description={{$description}}">
			<svg version="1.1"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
				 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
				<g>
					<path class="st0" d="M17.5,0C7.8,0,0,7.8,0,17.5S7.8,35,17.5,35S35,27.2,35,17.5S27.2,0,17.5,0z M21.3,10.1
						c-0.3,0-2.5,0.1-2.5,1.6s0,0.1,0,0.1v11.3c0,1.5,2.2,1.6,2.5,1.6v0.9h-7.6v-0.9c0.2,0,2.5-0.1,2.5-1.6V11.8c0-1.6-2.2-1.6-2.5-1.7
						V9.2h7.6L21.3,10.1z"/>
				</g>
			</svg>

			<label for="Instapaper">
				Instapaper
			</label>
		</a>
	</div>
</div>


















