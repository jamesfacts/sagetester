<header class="header-contain">
  <div class="row meta-wrap">

  	@php
  		$featured_img = App\featured_image_check();
  		$tall_featured_img = App\tall_featured_image_check();;
  	@endphp

  	@if ($featured_img)
	  	<div class="image-contain col-12 col-md-6">
	  		<div class="hero-image row">
	  			@if($tall_featured_img)
	  				<div class="image-fill tall col-12" style="background-image: url( {{ get_the_post_thumbnail_url(get_the_ID(), 'w700')}} );"></div> <!-- end image fill -->
		  			<div class="caption wf hidden-md-up col-12">@featured_img_caption</div>
		  		@else
						<div class="image-fill col-12" style="background-image: url( {{ get_the_post_thumbnail_url(get_the_ID(), 'w700')}} );"></div> <!-- end image fill -->
		  			<div class="caption wf hidden-md-up">@featured_img_caption</div>
		  		@endif



	  		</div> <!-- end image row -->
	  	</div>
	  <div class="meta col-12 col-md-6 text-center">
  	@else
			<div class="meta col-12 text-center no-img">
		@endif
  			<div class="stick">
	  		@php $column = @column_title @endphp
	  		@if($column)
					<div class="column-title"><a href="{{ home_url( '/' . $column->slug ) }}">{!! $column->name !!}</a></div>
				@endif
	  			<div class="author-meta"><span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</span>,
					<span class="date">&nbsp;@conditional_date</span>
	  			</div>
	  		</div>

	  		<h1 class="entry-title">{!! get_the_title() !!}</h1>
				@if(@subtitle)
					<h2 class="entry-subtitle">@subtitle</h2>
				@endif
  		</div>
  </div>
  @if ($featured_img)
	  <div class="row caption-wrap">
	  	<div class="caption wf hidden-sm-down col-6">@featured_img_caption</div>
	  </div>
  @endif
</header>
