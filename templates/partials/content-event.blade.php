<article 
	@php
		global $wp_query;

		$classes = '';
		$dateNow = new DateTime();
		$startDate = DateTime::createFromFormat('U', get_post_meta(get_the_ID(), 'event_start', true) );

		if($startDate < $dateNow) {
			$classes .= 'col-12';
		} else {
			$classes .= 'col-10 offset-1 col-md-12 offset-md-0';
		}
		post_class([$classes]);
	@endphp
	>
	<section class="row">
		<div class="date-left col-lg-2 offset-lg-1 hidden-md-down">
			<span class="date-dot">@php(the_date('m.d.Y'))</span>	
		</div>
		<div class="image-fill clickable col-12 col-sm-6 col-md-4 offset-md-2 offset-lg-0"
			@if(has_post_thumbnail()) 
				style="background-image: url({{ the_post_thumbnail_url('w500') }});">
			@else 
				style="background-image: url({{ get_template_directory_uri() . '/dist/images/tech-climbing.jpg' }});">
			@endif
			<a href="{{ the_permalink() }}" class="full-coverage" alt="{{ get_the_title() }}">&nbsp;</a>	
		</div>
		<header class="meta col-12 col-sm-6 col-md-5 col-lg-4">

			<div class="details">
				<h3 class="hed"><a href="{{ get_permalink() }}">@title</a></h3>
				<span class="city">{!! get_post_meta(get_the_ID(), 'event_location_city', true) !!}<span class="hidden-md-up">,</span></span>
				<span class="date hidden-md-up">@php(the_date('F j, Y'))</span>	
			</div>
			<p class="hidden-lg-down excerpt">{!! get_the_excerpt( get_the_ID() ) !!}</p>
				
		</header>

	</section> 
</article>