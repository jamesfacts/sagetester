<article 
	@php
		global $wp_query;
		$classes = 'col-10 offset-1 col-sm-12 offset-sm-0 type-post';
		$current_post = $bladeQuery->current_post ? $bladeQuery->current_post : $wp_query->current_post;

		if ($current_post === 0 || $current_post % 2 == 0) {
			$classes .= ' col-md-5 offset-md-2';
		} else {
			$classes .= ' col-md-5';
		}
		post_class([$classes]);
	@endphp
	>
	<section class="row">
		<div class="image-fill clickable col-12 col-sm-6" 
			@if(has_post_thumbnail()) 
				style="background-image: url({{ the_post_thumbnail_url('w350') }});">
			@else 
				style="background-image: url( {{ home_url( '/assets/') . '/search-ph1.jpg' }});">
			@endif
			<a href="{{ the_permalink() }}" class="full-coverage" alt="{{ get_the_title() }}">&nbsp;</a>	
		</div>
		<header class="meta col-12 col-sm-6">
			@php($contentType = App\get_content_type())
			
				@if( $contentType == 'page')
					<h3 class="hed"><a href="{{ get_permalink() }}"><em>Baffler</em> @title</a></h3>
				@else
					<h3 class="hed"><a href="{{ get_permalink() }}">@title</a></h3>
					@php 
						$cat_id = get_query_var('cat');
						if ( get_term_meta( $cat_id, '_column' )[0] !== '1' )
							$column = App\get_column_title();
					@endphp
			
					<div class="column-title">{!! $contentType !!}</div>
				
					<span class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</span>
						@if( $contentType != 'contributor')
							<span class="date">@conditional_date</span>
						@endif
				@endif
		</header>

	</section> 
</article>