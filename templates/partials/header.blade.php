

<header class="banner">

<nav class="navbar navbar-toggleable-sm fixed-top">

	<div class="hidden-md-up sm-header-wrap">
				
		<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav" aria-expanded="false" aria-label="Toggle navigation">


			<svg version="1.1" class="burger"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
				 x="0px" y="0px" width="27.3px" height="30px" viewBox="0 0 27.3 30" style="enable-background:new 0 0 27.3 30;"
				 xml:space="preserve">
					<path d="M27.3,30H0v-4.6h27.3V30z M27.3,12.7H0v4.6h27.3V12.7z M27.3,0H0v4.6h27.3V0z"/>
			</svg>

			<svg class="x-spot" version="1.1"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
				 x="0px" y="0px" width="22.5px" height="22.5px" viewBox="0 0 22.5 22.5" style="enable-background:new 0 0 22.5 22.5;"
				 xml:space="preserve">
					<polygon points="19.3,0 11.3,8 3.3,0 0,3.3 8,11.3 0,19.3 3.3,22.5 11.3,14.5 19.3,22.5 22.5,19.3 14.5,11.3 
						22.5,3.3 		"/>
			</svg>

		</button>		
			
		<div class="col-12 logo">
			<a href="{{ home_url( '/' ) }}" title="The Baffler Home">
				<svg version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
					 x="0px" y="0px" width="244px" height="32px" viewBox="0 0 244 32" style="enable-background:new 0 0 244 32;"
					 xml:space="preserve">
					<path d="M176.2,0h28.5v4.9h-22.8V14H200v4.7h-18.2V27h22.8v4.9h-28.5L176.2,0z M34.8,0c7.5,0,11.1,3.3,11.1,8.2v0.6
							c0,3.5-2,6.3-6.1,7.1c4.6,0.8,6.6,3.9,6.6,6.7v0.9c0,5-4,8.4-11.4,8.4H14.9V0H34.8z M34.8,18.1H20.6v9.1h14.6
							c3.9,0,5.3-1.9,5.3-4.2v-0.6C40.5,19.9,38.8,18.1,34.8,18.1L34.8,18.1z M35.1,4.8H20.6v9.1h14.1c3.9,0,5.7-1.9,5.7-4.4V9
							C40.4,6.7,39,4.8,35.1,4.8L35.1,4.8z M53.5,31.9h-5.9L62.4,0h5l15.1,31.9h-6l-3.6-8H57L53.5,31.9z M58.9,19.6h11.9l-6-13.2
							L58.9,19.6z M125,4.9h22.8V0h-28.5v31.9h5.7V18.7h18.2V14H125V4.9z M86.2,31.9h5.7V18.7H110V14H91.8V4.9h22.8V0H86.2L86.2,31.9z
							 M165.4,27h-22.1v4.9h27.8V0h-5.7L165.4,27z M232,18l12,14h-7l-11.2-13.2h-10.4v13.1h-5.7V0h17.8c8.6,0,12.1,3.3,12.4,8.6V9
							C240,14.6,237.6,17.7,232,18z M228.4,14.5c3.9,0,5.8-2,5.8-4.6V9.1c0-2.2-1.6-4.3-6-4.3h-12.8v9.7L228.4,14.5z M3.2,9.4h1.6V1.5
							h3.2V0.1H0v1.4h3.2V9.4z M7.8,11.2H6.1v3.9H1.9v-3.9H0.3v9.3h1.6v-4h4.2v4h1.7V11.2z M7.8,23.9v-1.4H0.3v9.3h7.5v-1.4H1.9v-2.7
							h4.5v-1.4H1.9v-2.4H7.8z"/>
				</svg>
			</a>
		</div>
			
  	
	</div>


	
	  <div class="collapse navbar-collapse " id="primaryNav">
			<div class="nav-contain">
			
				<span class="hidden-sm-down search-toggle">@include('partials.search-svg')</span>

				<form role="search" method="get" class="search-sm hidden-md-up" action="{{ home_url( '/' ) }}">
				  <label class="inline-search-sm">
				    <span class="screen-reader-text">Search for:</span>
				    <div class="mag-wrap">@include('partials.search-svg')</div>
				    <input type="search" class="search-field" placeholder="Search. . ." value="" name="s">
				  </label>
				</form>
		    
		    
		    @if (has_nav_menu('primary_navigation'))
		      {!! wp_nav_menu($mainNavArgs) !!}
		    @endif
		    
		    @include('partials.logo-svg-md')


			</div>
	  </div>
	
</nav>
<section class="hidden-sm-down search-wide push-up fixed-top row">
	<form role="search" method="get" class="col-md-12 col-lg-10 offset-lg-1 search-md" action="{{ home_url( '/' ) }}">
	  <label class="inline-search-md">
	    <span class="screen-reader-text">Search for:</span>
	    <input type="search" class="search-field" placeholder="Search. . ." value="" name="s">
				<svg class="x-spot search-toggle" version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
					 x="0px" y="0px" width="22.5px" height="22.5px" viewBox="0 0 22.5 22.5" style="enable-background:new 0 0 22.5 22.5;"
					 xml:space="preserve">
						<polygon points="19.3,0 11.3,8 3.3,0 0,3.3 8,11.3 0,19.3 3.3,22.5 11.3,14.5 19.3,22.5 22.5,19.3 14.5,11.3 
							22.5,3.3 		"/>
				</svg>
				<p>Press return to see results</p>
	  </label>
	</form>
</section>



</header>




