<div class="row">
<h2 class="announcement">You Might Also Enjoy</h2>
</div>
<div class="row stories">
	@php 
		$displayed = [];
		$displayed[] = get_the_ID();
		$source = basename(get_permalink());
		$cats = get_the_category(get_the_ID());
		$cat = $cats[0];
		$q_type = '';
		$s_content_type = wp_get_object_terms( get_the_ID(), 'content_type' )[0]->slug;	
	@endphp

	@if( in_array( $s_content_type, ['salvos', 'outbursts', 'odds-n-ends', 'ancestors', 'intros-and-manifestos']) || get_post_type( get_the_ID() ) == 'post' )
		@php $q_type = 'NOT IN'; @endphp
	@else
		@php $q_type = 'IN';	@endphp
	@endif

	@php
		$offset = rand(0,100);
		$args = ['offset' => $offset,
						 'post__not_in' => $displayed,
						 'posts_per_page' => '1',
						 'post_status' => 'publish',
						 'post_type' => ['article', 'post'],
						 'tax_query' => [
						 			['relation' => 'AND'],
			 						['taxonomy' => 'content_type', 'field' => 'slug', 'terms' => ['stories', 'poems'], 'operator' => $q_type],
			 						['taxonomy' => 'category', 'field' => 'slug', 'terms' => 'daily-bafflements', 'operator' => 'NOT IN']],
	 					 'orderby'     => 'modified',
	     			 'order'       => 'DESC' ];  


	@endphp


@for ($i = 0; $i < 3; $i++)
	<article class="col-10 offset-1 col-sm-8 offset-sm-2 col-md-4 offset-md-0">
		@query($args)
			<h3 class="title"><a href="{{ the_permalink() }}{{'?utm_campaign=enjoy&utm_source='}}{{ $source }}">@title</a></h3>
			<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
			<p class="excerpt hidden-md-up">{!!  get_the_excerpt( get_the_ID() ) !!}</p>
			<span class="content-type hidden-sm-down">@content_type</span>
		@endquery
		@php 
			$displayed[] = get_the_ID(); 
			$args['offset'] = rand(0,100);
		@endphp
	</article>
@endfor
</div>