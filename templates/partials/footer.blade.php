


<footer class="footer-frame">
  <div class="container-fluid footer-contain row">
     
     <div class="col-md-5 col-12 logo-menu">
     	<div class="row hidden-sm-down wide-logo">
     		<svg version="1.1" class="horz"
						 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
						 x="0px" y="0px" width="244px" height="32px" viewBox="0 0 244 32" style="enable-background:new 0 0 244 32;"
						 xml:space="preserve">
					<g>
						<path class="st0" d="M176.2,0h28.5v4.9h-22.8V14H200v4.7h-18.2V27h22.8v4.9h-28.5L176.2,0z M34.8,0c7.5,0,11.1,3.3,11.1,8.2v0.6
									c0,3.5-2,6.3-6.1,7.1c4.6,0.8,6.6,3.9,6.6,6.7v0.9c0,5-4,8.4-11.4,8.4H14.9V0H34.8z M34.8,18.1H20.6v9.1h14.6
									c3.9,0,5.3-1.9,5.3-4.2v-0.6C40.5,19.9,38.8,18.1,34.8,18.1L34.8,18.1z M35.1,4.8H20.6v9.1h14.1c3.9,0,5.7-1.9,5.7-4.4V9
									C40.4,6.7,39,4.8,35.1,4.8L35.1,4.8z M53.5,31.9h-5.9L62.4,0h5l15.1,31.9h-6l-3.6-8H57L53.5,31.9z M58.9,19.6h11.9l-6-13.2
									L58.9,19.6z M125,4.9h22.8V0h-28.5v31.9h5.7V18.7h18.2V14H125V4.9z M86.2,31.9h5.7V18.7H110V14H91.8V4.9h22.8V0H86.2L86.2,31.9z
									 M165.4,27h-22.1v4.9h27.8V0h-5.7L165.4,27z M232,18l12,14h-7l-11.2-13.2h-10.4v13.1h-5.7V0h17.8c8.6,0,12.1,3.3,12.4,8.6V9
									C240,14.6,237.6,17.7,232,18z M228.4,14.5c3.9,0,5.8-2,5.8-4.6V9.1c0-2.2-1.6-4.3-6-4.3h-12.8v9.7L228.4,14.5z M3.2,9.4h1.6V1.5
									h3.2V0.1H0v1.4h3.2V9.4z M7.8,11.2H6.1v3.9H1.9v-3.9H0.3v9.3h1.6v-4h4.2v4h1.7V11.2z M7.8,23.9v-1.4H0.3v9.3h7.5v-1.4H1.9v-2.7
									h4.5v-1.4H1.9v-2.4H7.8z"/>
					</g>
				</svg>
     	</div>
     	<div class="row copy">
				<div class="copyright col-md-6 hidden-sm-down">
					<span class="date text-center">
						{!! App\vert_text(date('Y')); !!}
					</span>
					<span class="title">
						&copy; The Baffler
					</span>
				</div>
				<div class="menu col-md-6 col-sm-8 ">
     			@if (has_nav_menu('footer_navigation'))
	        	{!! wp_nav_menu($footerNavArgs) !!}
	      	@endif					
				</div>
	
     	</div>

 	 	</div> <!-- end logo menu -->
     

		<div class="col-md-5 offset-md-1 merch hidden-sm-down">
			@php(dynamic_sidebar('sidebar-footer'))
		</div>  <!-- end merch -->

		<div class="col-md-1 offset-md-0 col-sm-8 offset-sm-2 social">
			
				<svg version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
					 x="0px" y="0px" width="140.4px" height="126px" viewBox="0 0 140.4 126" style="enable-background:new 0 0 140.4 126;"
					 xml:space="preserve" class="hidden-md-up corner-logo">


				<g>
					<path class="st0" d="M140.4,126h-8.6v-1.4h3.5V117h1.6v7.6h3.5V126z M134,111.6h4.1v3.8h1.6v-9h-1.6v3.9H134v-3.9h-1.6v9h1.6
						V111.6z M139.7,95.4h-7.4v1.4h5.7v2.6h-4.4v1.3h4.4v2.3h-5.7v1.4h7.4V95.4z M26,59.9h-7.9V42h-4.5v17.9H4.8V37.7H0.1v27.6h30.5
						V37.7H26V59.9z M97.4,103.5c0-4.8,3.8-8.1,11-8.1h18.4v30.5h-18.2c-7.2,0-10.8-3.2-10.8-7.8v-0.6c0-3.3,2-6,5.9-6.8
						c-4.5-0.7-6.3-3.7-6.3-6.4V103.5z M121.2,112.6h-12.6c-3.8,0-5.5,1.8-5.5,4.2v0.6c0,2.2,1.4,4,5.1,4h13L121.2,112.6z M103.1,104.5
						c0,2.4,1.6,4.1,5.5,4.1h12.7v-8.8h-13c-3.8,0-5.1,1.8-5.1,4L103.1,104.5z M78.4,125.9L63.8,95.4h5.9l3.5,7.6h15.4l3.4-7.6h5.7
						l-14.4,30.5H78.4z M75.1,107.2l5.9,12.6l5.7-12.6H75.1z M0.1,126h30.5v-5.5H18v-20h-4.5v20H4.8V96.3H0.1L0.1,126z M61.8,95.4h-5.4
						V108H38.5v4.5h17.9v8.7H34.2v4.7h27.6V95.4z M30.6,100.5V70H0.1v4.6h25v25.8H30.6z M30.5,33.2H0V15.9c0-8.4,3.1-11.7,8.2-12h0.5
						c6.6,0,8.3,2.3,8.6,7.7L30.6,0v6.3L18,17v10.7h12.5L30.5,33.2z M13.9,15c0-3.8-1.9-5.6-4.3-5.6H8.7c-2.1,0-4.1,1.5-4.1,5.8v12.4
						h9.3L13.9,15z"/>
					
				</g>
				</svg>				
			
			<div class="icons">
				<a href="https://www.facebook.com/TheBafflerMagazine/"><svg class="facebook" version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
					 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
				<g><path class="st0" d="M0,17.5C0,27.2,7.8,35,17.5,35S35,27.2,35,17.5S27.2,0,17.5,0S0,7.8,0,17.5z M12.6,14.1h2v-2.1
							c0-2.8,1.2-4.4,4.4-4.4h2.7V11h-1.7c-1.3,0-1.4,0.5-1.4,1.4v1.7h3.1l-0.4,3.4h-2.7v9.9h-4.1v-9.9h-2L12.6,14.1z"/></g>
				</a></svg>

				<a href="https://twitter.com/thebafflermag"><svg class="twitter" version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
					 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
				<g><path class="st0" d="M17.5,35C27.2,35,35,27.2,35,17.5S27.2,0,17.5,0S0,7.8,0,17.5S7.8,35,17.5,35z M10.9,20.2
							c0.5,0.1,1.1,0.1,1.6-0.1c-1.7-0.4-3-1.9-3-3.7c0.3,0.1,0.6,0.2,0.8,0.3c0.3,0.1,0.6,0.1,0.8,0.1c-0.8-0.6-1.3-1.4-1.5-2.3
							c-0.2-0.9-0.1-1.9,0.4-2.7c1.9,2.3,4.7,3.8,7.7,3.9c0-0.2,0-0.3-0.1-0.5c-0.2-2.1,1.3-3.9,3.4-4.1c0.3,0,0.6,0,1,0
							c0.8,0.1,1.5,0.5,2,1c0.1,0.1,0.1,0.1,0.2,0.1c0.7-0.2,1.4-0.4,2-0.8l0.2-0.1c-0.1,0.4-0.3,0.8-0.6,1.1c-0.3,0.3-0.6,0.6-0.9,0.9
							v0.1c0.3-0.1,0.7-0.1,1-0.2s0.7-0.2,1-0.3c-0.3,0.4-0.6,0.8-1,1.2c-0.2,0.2-0.5,0.4-0.8,0.7c-0.1,0.1-0.1,0.1-0.1,0.2
							c0,1.1-0.1,2.2-0.4,3.2c-0.2,0.9-0.6,1.7-1,2.5c-0.4,0.7-0.8,1.3-1.3,1.8c-0.6,0.7-1.4,1.4-2.2,1.9c-0.7,0.4-1.4,0.8-2.2,1.1
							c-1.3,0.4-2.6,0.6-4,0.5c-1.1,0-2.2-0.3-3.3-0.7c-0.7-0.3-1.3-0.6-1.9-1c1.9,0.2,3.9-0.4,5.4-1.6C12.8,22.6,11.5,21.6,10.9,20.2z"
							/></g>
				</svg></a>
				<span class="credits"><a href="https://thebaffler.com/latest/redesign-announcement-baffler">Credits</a></span>
			</div>

			
		</div> <!-- end social -->


  </div>
</footer>

<!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/480141.js"></script>
<!-- End of HubSpot Embed Code -->





