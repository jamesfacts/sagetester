<article @php(post_class())>
	<header class="issue-meta">
		<section class="row">
			<div class="col-12 col-md-6 issue-cover">
				<div class="image-fill" style="background-image: url( {{ get_the_post_thumbnail_url(get_the_ID(), 'w700') }} );">
				
				</div>
			</div>
			<div class="col-12 col-md-6 issue-buttons">
				<div class="row text-center meta-wrap">
					<h4 class="issue-no hidden-sm-down">Issue @title</h4>
					<h1 class="title"><span class="hidden-md-up">@title—</span>{!! get_post_meta(get_the_ID(), '_issue_title', true) !!}</h1>
					<section class="buttons">
						<div class="col-md-6 offset-md-0 col-sm-8 offset-sm-2 col-10 offset-1">
							<a href="{{ home_url( '/subscribe' ) }}"><input type="submit" value="Subscribe" class="btn"></a>
						</div>
						<div class="col-md-6 offset-md-0 col-sm-8 offset-sm-2 col-10 offset-1">
							<a href="{!! get_post_meta(get_the_ID(), '_buy_link', true) !!}"><input type="submit" value="Buy" class="btn"></a>
						</div>
					</section>
				</div>
			</div>
		</section>

	</header>



  <section class="row entry-row">
    <div class="hidden-sm-down col-md-1 flag">
      <div class="text-center">
        @php
          $issue_date = get_the_date( 'F Y', get_the_ID() );
          echo App\vert_text($issue_date);
        @endphp
      </div>
    </div>
    <div class="col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-1 col-lg-8 offset-lg-1 col-xl-6 offset-xl-2 entry-content">
      @php(the_content())
    </div>
  </section>

	<section class="contents">
		<h2 class="toc-main-heading">Table of Contents</h2>


	<?php
		$types = ['salvos','outbursts','odds-and-ends','poems','stories','intros-and-manifestos','ancestors']; 
		foreach($types as $type) {
			$args = [
				'post_status' => 'publish',
				'post_type' => 'article',
				'tax_query' => [
					[ 'taxonomy' => 'content_type',
						'field' => 'slug',
						'terms' => $type	]
				],
				'order' => 'DESC',
				'orderby' => 'post_date',
				'posts_per_page' => -1,
				'meta_query' => [
					[	'key' => '_issue_id',
						'value' => get_the_ID()	]
				]
			];

			
							
			$the_query = new WP_Query($args);

			$content_type_tax = get_term_by('slug', $type, 'content_type');
			$typeTitle = $content_type_tax->name;
			
			if ($the_query->have_posts()) {

				echo '<section class="row overview overview-' . $type . ' "><h2 class="toc text-center heading-' . $type. '">' . $typeTitle . '</h2>';

				while ($the_query->have_posts()) : $the_query->the_post(); 
					
					if ($type != 'poems') { ?>
						<div class="col-12 col-md-3">
							<article class="row story">
								<div class="col-12 col-sm-6 col-md-12 image-fill" style="background-image: url( {{ get_the_post_thumbnail_url(get_the_ID(), 'w350') }} );">
									<a class="full-coverage" href="<?php echo the_permalink(); ?>">
									</a>
								</div>
								<div class="col-12 col-sm-6 col-md-12 meta">
									<h3 class="title"><a href="<?php echo the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
									<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>		
								</div>
							</article>		
							
						</div> 
					<?php } else { ?>
						<div class="col-12 col-md-3">
							<article class="row story">
								<div class="col-12 meta">
									<h3 class="title"><a href="<?php echo the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
									<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>		
								</div>
							</article>		
							
						</div> 

					<?php } ?>
				
				<?php endwhile; ?>

				</section>

			<?php } ?>
			
			<?php wp_reset_query(); 
		} ?>





	</section>

</article>
