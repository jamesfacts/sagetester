<div class="page-header col-12 col-md-11 offset-md-1">
	@php
		$headingText = ucfirst( App\title() );
		$subHed = App\catSubhed() ? App\catSubhed() : '';
	@endphp
	@if( is_post_type_archive('issue') )
		@php($headingText = 'Back Issues')
	@elseif ( is_home() )
		@php($headingText = 'Latest Posts')
		@php($subHed = 'Daily broadsides from <em>The Baffler</em>’s just-in-time corps of knowledge workers')
	@elseif( is_post_type_archive('book') )
  	@php($headingText = 'Baffler Publishing')
  @elseif( is_post_type_archive('event') )
  	@php($headingText = 'All Events')
	@endif
	<h1 class="heading" style="background-color:red;">{!! $headingText !!}</h1>
	<hr class="split">
	@if (@subHed !== '')
		<h5 class="subhed">{!! $subHed !!}</h5>
	@endif

</div>
