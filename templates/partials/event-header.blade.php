<header class="header-contain">
  <div class="row meta-wrap">

  	@php 
  		$featured_img = App\featured_image_check();
  		$tall_featured_img = App\tall_featured_image_check();;
  	@endphp
		
  	@if ($featured_img)
	  	<div class="image-contain col-12 col-md-6">
	  		<div class="hero-image row">
	  			@if($tall_featured_img)
	  				<div class="image-fill tall col-12" style="background-image: url( {{ get_the_post_thumbnail_url(get_the_ID(), 'w700')}} );"></div> <!-- end image fill -->
		  			<div class="caption wf hidden-md-up col-12">@featured_img_caption</div>
		  		@else
						<div class="image-fill col-12" style="background-image: url( {{ get_the_post_thumbnail_url(get_the_ID(), 'w700')}} );"></div> <!-- end image fill -->
		  			<div class="caption wf hidden-md-up">@featured_img_caption</div>
		  		@endif
		  			
		  		
		  		
	  		</div> <!-- end image row -->
	  	</div>
	  <div class="meta col-12 col-md-6 text-center">	
  	@else
			<div class="meta col-12 text-center no-img">
		@endif
  			<div class="stick">
  				@php
  					$startDate = DateTime::createFromFormat('U', get_post_meta(get_the_ID(), 'event_start', true) );
						$prettyDate = $startDate->format('l, F j, Y');
						$prettyTime = $startDate->format('g:ia');
						$prettyTime = preg_replace(['/am/','/pm/'], ['a.m.', 'p.m.'], $prettyTime);
  				@endphp
					<span class="date">{{ $prettyDate }}</span>
					<span class="date time">{{ $prettyTime }}<span>
	  			
	  		</div>

	  		<h1 class="entry-title">{!! get_the_title() !!}</h1>
	  		@php($location = get_post_meta(get_the_ID(), 'event_location', true) . ", " . get_post_meta(get_the_ID(), 'event_location_city', true) . ", " . get_post_meta(get_the_ID(), 'event_location_state', true))
				@if($location)
					<h2 class="entry-subtitle">{{$location}}</h2>
				@endif
  		</div>
  </div>
  @if ($featured_img)
	  <div class="row caption-wrap">
	  	<div class="caption wf hidden-sm-down col-6">@featured_img_caption</div>
	  </div>
  @endif
</header>




