<div class="row">
	<h2 class="announcement">Further Reading</h2>
</div>
<div class="row stories">
	@php 
		$displayed = [];
		$displayed[] = get_the_ID();
		$source = basename(get_permalink());
	@endphp

	@php
		$offset = rand(0,10);
		$args = ['post__not_in' => $displayed,
						 'posts_per_page' => '1',
						 'offset' => $offset,
						 'post_status' => 'publish',
						 'post_type' => ['article', 'post'],
						 'tax_query' => [
			 						['taxonomy' => 'category', 'field' => 'slug', 'terms' => 'daily-bafflements', 'operator' => 'NOT IN']],
 						 'meta_query' => [['key' => '_top_rollout', 'value' => 1]],
	 					 'orderby'     => 'modified',
	     			 'order'       => 'DESC' ];  


	@endphp


@for ($i = 0; $i < 3; $i++)
	@query($args)
	<article class="col-12 row">
		<div class="col-md-3 hidden-sm-down date-wrap">
			<span class="date">&nbsp;@conditional_date</span>
		</div>
		<div class="col-6 col-md-3 image-fill"
			style="background-image: url( {{the_post_thumbnail_url('w350')}} );">
			<a href="{{ the_permalink() }}{{'?utm_campaign=further&utm_source='}}{{ $source }}" class="full-coverage">
				&nbsp;
			</a>
		</div>
		<div class="col-6 meta">
			<div class="top-line">
				<h3 class="title"><a href="{{ the_permalink() }}{{'?utm_campaign=further&utm_source='}}{{ $source }}">@title</a></h3>
				<h4 class="author">{!! App\baffler_author_meta(get_the_ID(), true, true) !!}</h4>
			</div>
			<p class="excerpt">{!!  get_the_excerpt( get_the_ID() ) !!}</p>
		</div>
			
	</article>
	@endquery
	@php 
		$displayed[] = get_the_ID(); 
		$args['offset']++;
	@endphp
@endfor
	
</div>