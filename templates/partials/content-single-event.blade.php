
@include('partials/event-header')


<article @php(post_class())>
  <section class="row entry-row">
    <div class="hidden-sm-down col-md-1 flag">
      <div class="text-center">
        @php 
          $content_type = App\get_content_type();
          echo App\vert_text($content_type);
        @endphp
      </div>
    </div>
    <div class="col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-1 col-lg-8 offset-lg-1 col-xl-6 offset-xl-2 entry-content">
      @php(the_content())
      @if ( get_post_meta( get_the_id() )["_hs_form_id"] ) 
      <div class="about-hs">
        <!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]-->
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
          hbspt.forms.create({ 
            css: '',
            portalId: '480141',
            formId: '{{ get_post_meta( get_the_id() )["_hs_form_id"][0] }}'
          });
        </script>
      </div>
      @endif
    </div>
  </section>
</article>

<article class="further-info">
  <section class="icon-contain">
    <div class="col-12 offset-0 col-sm-10 offset-sm-1">
      @include('partials/share-icons')
    </div>
  </section>
 
  <section class="single-email row"> 
    @include('partials/emember-horz')
  </section>

</article>



