

@php $s_post_type = get_post_type( get_the_ID() ) @endphp

@if( $s_post_type == 'post' ) 
  @include('partials/latest-header')
@elseif( get_post_type( get_the_ID() ) == 'article' ) 
  @php $s_content_type = wp_get_object_terms( get_the_ID(), 'content_type' )[0]->slug @endphp
  @if ( $s_content_type == 'poems' )
    @include('partials/poem-header')
  @elseif ( $s_content_type == 'stories' )
    @include('partials/story-header')
  @elseif( $s_content_type == 'salvos' || $s_content_type == 'outbursts' || $s_content_type == 'odds-and-ends' )
    
      @php 
        $s_post_date = get_the_date( 'Y-m-d', get_the_ID() );
        $s_post_date_obj = strtotime( $s_post_date );
      @endphp

      @if( $s_post_date_obj < strtotime('-18 months') )
        @include('partials/archive-header')
      @else
        @include('partials/salvo-header')
      @endif 

  @else
    @include('partials/archive-header')
  @endif  

@endif

<article @php(post_class())>
  <section class="row entry-row">
    <div class="hidden-sm-down col-md-1 flag">
      <div class="text-center">
        @php 
          $content_type = App\get_content_type();
          echo App\vert_text($content_type);
        @endphp
      </div>
    </div>
    <div class="col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-1 col-lg-8 offset-lg-1 col-xl-6 offset-xl-2 entry-content">
      @php(the_content())
    </div>
  </section>
</article>

<article class="further-info">
  @php 
    $author_bio_tag = App\get_author_bio_tag();
  @endphp
  @if($author_bio_tag)
    <section class="row">
      <div class="col-12 offset-0 col-sm-10 offset-sm-1">
        @include('partials/author-bio')
      </div>
    </section>
  @endif
  <section class="icon-contain">
    <div class="col-12 offset-0 col-sm-10 offset-sm-1">
      @include('partials/share-icons')
    </div>
  </section>
  <section class="entry-enjoy">
    <div class="col-12 offset-0 col-sm-10 offset-sm-1">
      @include('partials/enjoy')
    </div>
  </section>


  <section class="single-email row"> 
    @include('partials/emember-horz')
  </section>


  <section class="rollout-recos">
    <div class="col-12 col-lg-10 offset-lg-1">
      @include("partials/rollout-recos")
    </div>
  </section>
</article>



