{{--
  Template Name: About Pages
--}}


@extends('layouts.base')
	@section('content')
		<section class="row page-wrap">
	  	@while(have_posts()) @php(the_post())
	    	@include('partials.page-header-about')
	    	<aside class="image-col col-12 col-sm-10 offset-sm-1 col-md-3 offset-md-0">
	      	@if(App\featured_image_check())
	      		<img src="{{the_post_thumbnail_url('w350')}}">
      		@else
      			<img src="{{ get_template_directory_uri() . '/dist/images/baffler-stack.png' }}">
    			@endif
	    	</aside>
	    	<article class="about-body entry-content col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0">
	    	@include('partials.content-page')
	    	@if ( get_post_meta( get_the_id() )["_hs_form_id"] ) 
					<div class="about-hs">
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
						  hbspt.forms.create({ 
						    css: '',
						    portalId: '480141',
						    formId: '{{ get_post_meta( get_the_id() )["_hs_form_id"][0] }}'
						  });
						</script>
					</div>
	    	@endif
	    	</article>
				
        <aside class="sidebar col-md-3">
          @include('partials.sidebar')
        </aside>
        
	  	@endwhile
  	</section>
	@endsection


