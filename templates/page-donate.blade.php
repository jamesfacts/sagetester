@extends('layouts.base')

@section('content')
	@include('partials.donate-style')
	<section class="row page-wrap">
		<section class="animation-canvas">
			<div class="theater"> <!-- 100% width -->
				<div class="left"></div> <!-- 10% width -->
				
				<div class="center-wrapper">

					<div class="center-bird"><!-- 80% width -->

						<div class="bird-hand">	

							<div class="bird-flying not-ie-speech">			
							</div> 
						</div>

					</div>

					<div class="center-buck"> <!-- 80% width -->
					
						<div class="left-pass">			
						</div>

						<div class="dollar-pass">			
						</div>

						<div class="right-pass">			
						</div>				

					</div> <!-- end center bird and buck-->
				
				</div> <!-- end center wrapper-->

				<div class="right"></div> <!-- 10% width -->
			</div>
		</section>

  	@while(have_posts()) @php(the_post())
    	<article class="about-body col-12 col-sm-10 col-md-8">
    	@include('partials.content-page')
    	</article>
  	@endwhile
	</section>
@endsection
