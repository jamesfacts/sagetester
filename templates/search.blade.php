@extends('layouts.base')

@section('content')
  <section class="row page-wrap">
  
  @include('partials.search-header')

  @if (!have_posts())
    <section class="no-results">
      <div class="alert alert-warning col-12 col-md-11 offset-md-1">
        {{  __('Sorry, no results were found.', 'sage') }}
      </div>
    </section>
  @endif

  @while(have_posts()) @php(the_post())
    @include ('partials.content-search')
  @endwhile

  {!! get_the_posts_navigation([
                                    'prev_text'   => 'older results',
                                    'next_text'   => 'newer results']) !!}

  </section>
@endsection
