@extends('layouts.base')

@if ( !is_front_page() )

	@section('content')
		<section class="row page-wrap">
	  	@while(have_posts()) @php(the_post())
	    	@include('partials.page-header-about')
	    	<aside class="image-col col-12 col-sm-10 offset-sm-1 col-md-3 offset-md-0">
	      	@if(App\featured_image_check())
	      		<img src="{{the_post_thumbnail_url('w350')}}">
      		@else
      			<img src="{{ get_template_directory_uri() . '/dist/images/baffler-stack.png' }}">
    			@endif
	    	</aside>
	    	<article class="about-body entry-content col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0">
	    		@include('partials.content-page')
	    	</article>		
        
	  	@endwhile
  	</section>
	@endsection
@else
	@section('content')
		@include('partials.front-page')
	@endsection
@endif

