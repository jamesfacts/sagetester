@extends('layouts.base')

@section('content')
	<section class="row page-wrap">
  @include('partials.page-header')
  
  <section class="not-found-img col-12 col-md-6 offset-md-3">
  	<img src="{{ get_template_directory_uri() . '/dist/images/Monster_NoFlash_838.gif' }}" alt="404 not found">
  	
  	<h4>Sorry, that page was not found. Try searching, or <a href="{{ home_url( '/' ) }}" title="The Baffler">return home</a>.</h4>
  </section>

	<section class="search-wide-body col-12 col-sm-10 offset-sm-1">
		<form role="search" method="get" class="search-md" action="{{ home_url( '/' ) }}">
		  <label class="inline-search">
		    <span class="screen-reader-text">Search for:</span>
		    <input type="search" class="search-field" placeholder="Search. . ." value="" name="s">
				<p class="hidden-md-down">Press return to see results</p>
		  </label>
		</form>
	</section>

  </section>
@endsection
