@extends('layouts.base')

  @section('content')

    <section class="row page-wrap">
    
        @include('partials.page-header')
        
        @if(is_post_type_archive('event'))
          @php($dateNow = new DateTime())
          @php($pastBreak = false)

          @while (have_posts()) @php(the_post())
            @if(!$pastBreak)
              @php( $startDate = DateTime::createFromFormat('U', get_post_meta(get_the_ID(), 'event_start', true) ) )
                @if ($startDate < $dateNow) 
                    <h2 class="past-break">Past Events</h2>
                    @php($pastBreak = true)
                @endif

            @endif

            @include ('partials.content-event')
          @endwhile

        @else
          @if (!have_posts())
            <div class="alert alert-warning">
              {{ __('Sorry, no results were found.', 'sage') }}
            </div>
            {!! get_search_form(false) !!}
          @endif

          @php($showEmail = is_home() ? true : false)    

          @while (have_posts()) @php(the_post())
            @php  
              global $wp_query; 
              $post_count = $wp_query->current_post;
            @endphp
      
            @if ( $showEmail &&  ( in_array($post_count, [4, 16, 28] ) ) )
              <section class="blog-emember col-12 col-md-11 offset-md-1">
                <div class="row">
                @include ('partials.emember-horz')
              </div>
              </section>
              @include ('partials.content-'.(get_post_type() !== 'post' ? App\get_content_type() : get_post_format()))
            @else
              @include ('partials.content-'.(get_post_type() !== 'post' ? App\get_content_type() : get_post_format()))
            @endif
          @endwhile
        @endif

        @php( $post_type = get_post_type() )
        @if($post_type == 'article')
          @php($post_type = App\get_content_type())
        @elseif( $post_type == 'event' )
          @php($post_type = 'events')
        @elseif( $post_type == 'issue' )
          @php($post_type = 'issues')
        @else 
          @php($post_type = 'posts')
        @endif

        {!! get_the_posts_navigation([
                                    'prev_text'   => 'older ' . $post_type,
                                    'next_text'   => 'newer ' . $post_type]) !!}

    </section>
  @endsection

